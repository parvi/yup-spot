angular.module('starter.controllers', ['pascalprecht.translate', 'ionic',
  'ngCordova'])
  // Accueil Controller
  .controller('DashCtrl', function ($scope) {
    console.log('DashCtrl');
    $scope.data = {};
    $scope.data.isconn = localStorage.getItem('isconn');
  })
  .controller('AccountCtrl', function ($scope, $translate, $ionicLoading) {
    $scope.showw = false;

    $scope.curlang = $translate.use();
    $scope.changeLanguage = function (key) {
      $ionicLoading.show({ content: 'Loading', animation: 'fade-in', showBackdrop: true, maxWidth: 200, showDelay: 0, duration: 3000 });
      $translate.use(key);
      $scope.curlang = key;
      $ionicLoading.hide()
      localStorage.setItem('preferredLanguage', key);
    };
  })
  .controller('AppCtrl', function ($scope,
    $ionicModal,
    $timeout,
    $state,
    $translate,
    $http,
    $ionicPopup,
    $ionicLoading,
    urlPhp,
    urlJava) {
    $scope.menu = true;
    $scope.scroll = false;
    $scope.menutab = false;
    $scope.data = {};

    if (localStorage.getItem('loggedin_name') == null || localStorage.getItem('loggedin_name') == 'null') {
      console.log('non autoriser')
      $scope.scroll = false;
    } else {
      $scope.scroll = true;
      // $scope.menu = true;
      console.log('autoriser')
    }
    $scope.majStat = function () {
      if ($scope.menutab) {
        //if($scope.scroll){
        $scope.menu = true;
        $scope.menutab = false;
        // }
      } else {

        $scope.menutab = true;
        $scope.menu = false;
      }
    }


    if (localStorage.length != 0) {
      $scope.connectedyet = true;
      $scope.sessionloginid = localStorage.getItem('loggedin_id');
      $scope.sessionlogininame = localStorage.getItem('loggedin_name');
      $scope.sessionpassword = localStorage.getItem('loggedin_password');
      $scope.sessionloginiduser = localStorage.getItem('loggedin_iduser');
      $scope.sessionprofile = localStorage.getItem('loggedin_profil')

      sessionStorage.setItem('loggedin_id', $scope.sessionloginid);
      sessionStorage.setItem('loggedin_name', $scope.sessionlogininame);
      sessionStorage.setItem('loggedin_password', $scope.sessionpassword);
      sessionStorage.setItem('loggedin_iduser', $scope.sessionloginiduser);
      sessionStorage.setItem('loggedin_profil', $scope.sessionprofile);

    }
    // Form data for the login modal
    $scope.loginData = {};
    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.modal = modal;
    });
    // Triggered in the login modal to close it
    $scope.closeLogin = function () {
      $scope.modal.hide();
    };
    // Open the login modal
    $scope.login = function () {
      $scope.modal.show();
    };

    // Perform the login action when the user submits the login form
    $scope.doLogin = function () {
      console.log('Doing login', $scope.loginData);

      // Simulate a login delay. Remove this and replace with your login
      // code if using a login system
      $timeout(function () {
        $scope.closeLogin();
      }, 1000);
    };

    // Enregistrement de la fiche de visite
    //Gerer les donnees locales: Synchronisation
    $scope.synchronDataLocal = function () {
      if (window.Connection) {

        if (navigator.connection.type == Connection.NONE) {

        } else {

          var flocal = angular.fromJson(localStorage.getItem('ficheSauvegarde'));
          if (flocal == null || flocal.length == 0) {
            console.log('Pas de donnees locales')
            $ionicLoading.hide();
          } else {
            $translate('header_title_synchrone').then(function (header) {
              $translate('content_label_synchrone').then(function (content) {
                $translate('content_label_synchrone_nb_fiche').then(function (nb) {
                  $translate('content_label_synchrone_nb_suggetion').then(function (sugest) {
                    $translate('alert_button_oui').then(function (oui) {
                      $translate('alert_button_non').then(function (non) {
                        var echec = [];
                        $ionicPopup.show({
                          title: header,
                          template: content + "<br/>" + nb + flocal.length + "<br/>" + sugest,
                          scope: $scope,
                          buttons: [
                            {
                              text: non,
                              type: 'button-assertive',
                              onTap: function (e) {
                                return false;
                              }
                            },
                            {
                              text: oui,
                              type: 'button-energized',
                              onTap: function (e) {
                                return true;
                              }
                            }]
                        }).then(function (result) {
                          if (!result) {

                          } else {
                            // console.log(flocal[0])
                            var url = urlJava.getUrl()
                            $ionicLoading.show({ content: 'Tentative de synchronisation', animation: 'fade-in', showBackdrop: true, maxWidth: 800, showDelay: 0, timeout: 10000 });
                            for (var i = 0; i < flocal.length; i++) {
                              $scope.data.value = flocal[i];
                              $http.post(url + '/yup/saveFiche', $scope.data.value).then(function (res) {
                              }).catch(function (error) {
                                echec.push(flocal[i])
                                $ionicLoading.hide();
                                alert('Synchro echouée');
                              });
                            }
                            $ionicLoading.hide();
                            if (echec.length == 0) {
                              var init = [];

                              $translate('alert_header_reussi').then(function (header) {
                                $translate('alert_content_reussi').then(function (content) {
                                  $ionicPopup.confirm({
                                    title: header,
                                    content: content,
                                    buttons: [
                                      {
                                        text: 'OK',
                                        type: 'button-energized',
                                        onTap: function (e) {
                                          return true;
                                        }
                                      }
                                    ]
                                  })
                                    .then(function (result) {
                                      if (result) {
                                        localStorage.setItem('ficheSauvegarde', angular.toJson(init))
                                        $scope.synchronDataLocalProspect();
                                      } else {
                                        localStorage.setItem('ficheSauvegarde', angular.toJson(init))
                                        $scope.synchronDataLocalProspect();
                                      }
                                    });
                                })
                              })
                            } else {
                              localStorage.setItem('ficheSauvegarde', angular.toJson(echec))
                            }
                          }
                        })

                      })
                    })

                  });

                })
              })
            })


          }
        }

      }
    }
    $scope.synchronDataLocal();
    //Gerer les donnees locales: Synchronisation

    $scope.synchronDataLocalProspect = function () {
      //  $ionicLoading.show({ content: 'Tentative de synchronisation', animation: 'fade-in', showBackdrop: true, maxWidth: 800, showDelay: 0, timeout: 10000 });
      var flocal = angular.fromJson(localStorage.getItem('prospectsSauvegarde'));
      if (flocal == null || flocal.length == 0) {
        console.log('Pas de donnees locales')
        //  $ionicLoading.hide();
      } else {
        //  var echec = [];

        $translate('header_title_synchrone').then(function (header) {
          $translate('content_label_synchrone').then(function (content) {
            $translate('content_label_synchrone_nb_fichep').then(function (nb) {
              $translate('content_label_synchrone_nb_suggetion').then(function (sugest) {
                $translate('alert_button_oui').then(function (oui) {
                  $translate('alert_button_non').then(function (non) {
                    var echec = [];
                    $ionicPopup.show({
                      title: header,
                      template: content + "<br/>" + nb + flocal.length + "<br/>" + sugest,
                      scope: $scope,
                      buttons: [
                        {
                          text: non,
                          type: 'button-assertive',
                          onTap: function (e) {
                            return false;
                          }
                        },
                        {
                          text: oui,
                          type: 'button-energized',
                          onTap: function (e) {
                            return true;
                          }
                        }]
                    }).then(function (result) {
                      if (!result) {

                      } else {
                        // console.log(flocal[0])
                        var url = urlPhp.getUrl();
                        $ionicLoading.show({ content: 'Tentative de synchronisation', animation: 'fade-in', showBackdrop: true, maxWidth: 800, showDelay: 0, timeout: 3000 });
                        for (var i = 0; i < flocal.length; i++) {
                          $http.post(url + '/prospect.php', flocal[i]).then(function (res) {
                            console.log('index :' + i + " " + res.data)
                          }).catch(function (error) {
                            echec.push(flocal[i])
                            console.log('echoue')
                            $ionicLoading.hide();
                            alert('Synchro echouée');
                          });
                        }
                        console.log('Taille apres la boucle')
                        console.log(echec)
                        $ionicLoading.hide();

                        if (echec.length == 0) {
                          var init = [];
                          console.log('tout est envoyé')
                          $translate('alert_header_reussi').then(function (header) {
                            $translate('alert_content_reussi').then(function (content) {
                              $ionicPopup.confirm({
                                title: header,
                                content: content,
                                buttons: [
                                  {
                                    text: 'OK',
                                    type: 'button-energized',
                                    onTap: function (e) {
                                      return true;
                                    }
                                  }]
                              })
                                .then(function (result) {
                                  if (result) {
                                    localStorage.setItem('prospectsSauvegarde', angular.toJson(init))
                                  } else {
                                    localStorage.setItem('prospectsSauvegarde', angular.toJson(init))
                                  }
                                });
                            })
                          })

                        } else {
                          console.log('Taille s il ya erreur')
                          console.log(echec)
                          localStorage.setItem('prospectsSauvegarde', angular.toJson(echec))
                        }
                      }
                    })
                  })
                })

              })
            })
          })
        })

      }
    }



  })

  .controller('MyApp', function ($scope, $translate) {
    $scope.reloadPage = function () {
      window.location.reload();
    }

  })

  // Login Controller
  .controller('LoginCtrl', function ($scope,
    $http,
    $ionicPopup,
    $state,
    $ionicLoading,
    $ionicHistory,
    $translate,
    $ionicPlatform,
    urlPhp) {


    $scope.user = {
      login: '',
      password: ''
    };
    //test connexion abou
    $scope.sowmenu = function () {

    }
    $scope.login = function () {
      if (window.cordova) {
        if (window.Connection) {

          if (navigator.connection.type == Connection.NONE) {

            $translate('alert_header_ofline').then(function (header) {

              $translate('alert_content_ofline_home').then(function (content) {

              });
            });

          } else {
            $scope.conn();
          }
        }
      } else {
        $scope.conn();
      }

    };
    $scope.conn = function () {
      var url = urlPhp.getUrl();
      $ionicLoading.show({ content: 'Loading', animation: 'fade-in', showBackdrop: true, maxWidth: 200, showDelay: 0, duration: 10000 });
      var str = url + "/login1.php?login=" + $scope.user.login + "&password=" + $scope.user.password;
      $http.get(str)
        .success(function (res) { // if login request is Accepted
          $ionicLoading.hide();
          // records is the 'server response array' variable name.
          $scope.user_details = res.records; // copy response values to user-details object.
          console.log($scope.user_details);
          sessionStorage.setItem('loggedin_id', $scope.user_details.idUtilisateursPointVent);
          sessionStorage.setItem('loggedin_name', $scope.user_details.login);
          sessionStorage.setItem('loggedin_password', $scope.user_details.password);
          sessionStorage.setItem('loggedin_iduser', $scope.user_details.idutilisateurs);
          sessionStorage.setItem('loggedin_profil', $scope.user_details.profil);
          console.log($scope.user_details.profil)
          localStorage.setItem('loggedin_id', $scope.user_details.idUtilisateursPointVent);
          localStorage.setItem('loggedin_name', $scope.user_details.login);
          localStorage.setItem('loggedin_password', $scope.user_details.password);
          localStorage.setItem('loggedin_iduser', $scope.user_details.idutilisateurs);
          localStorage.setItem('loggedin_profil', $scope.user_details.profil);
          localStorage.setItem('isconn', true);
          $ionicHistory.nextViewOptions({
            disableAnimate: true,
            disableBack: true
          });
          $translate('alert_connexion_reussi_header').then(function (header) {
            $translate('alert_connexion_reussi_content').then(function (content) {
              var alertPopup = $ionicPopup.alert({
                title: header,
                template: content + $scope.user_details.login + ' !'
              });
            });
          });
          //   $scope.menu = true;
          //   $scope.scroll = true;
          $state.transitionTo('app.bienvenue', {}, {
            reload: true,
            inherit: true,
            notify: true
          });
        }).error(function () { //if login failed
          $ionicLoading.hide();
          $translate('alert_connexion_lost_header').then(function (header) {
            $translate('alert_connexion_lost_content').then(function (content) {
              var alertPopup = $ionicPopup.alert({
                title: header,
                template: content
              });
            });
          });

        });
    }
  })
  // Fiche Visite Controller
  .controller('FicheVisiteCtrl', function (
    $scope,
    $http,
    $ionicPopup,
    $state,
    $stateParams,
    $location,
    $cordovaGeolocation,
    $ionicLoading,
    ChekConnect,
    $translate,
    ProfilUser,
    ListpaysByProfil,
    urlJava,
    urlPhp,
    ApiListZonesSuper,
    ApiListPointventeParZone,
    $filter
  ) {

    $scope.pointvente = [];
    $scope.data = {};
    $scope.data.pvchoisit = null;
    $scope.data.pvchoisitoffline = null;
    $scope.data.nosubmit = false;
    $scope.data.longitude = null;
    $scope.data.latitude = null;
    $scope.distance = 0;
    $scope.traitementLunch = false
    $scope.data.dblclk = 0;
    $scope.data.profile = 'limite';
    $scope.data.listpays;
    $scope.data.payschoisit = null
    $scope.data.payschoisioffline = null;
    $scope.data.listZones;
    $scope.data.zonechoisit;

    $scope.data.reponse = "oui"
    //Tester la connectiviteee
    $scope.testProfile = function () {

      if (localStorage.getItem('loggedin_profil') == 'Codir YUP Mgt' || localStorage.getItem('loggedin_profil') == 'Direction Commerciale YUP Mgt'
        || localStorage.getItem('loggedin_profil') == 'Marketing YUP Mgt' || localStorage.getItem('loggedin_profil') == 'Call Center YUP Mgt'
        || localStorage.getItem('loggedin_profil') == 'Administrateur Maintenance') {
        // $scope.data.profile = 'super';
        $scope.data.profile = 'super';
      }
      // return profil;
    }
    $scope.goToPlusInfoPv = function (item) {
      console.log(item);
      var poinvente =item;
      $state.go('app.pointvente', {
        pvt: poinvente
      });
    }
    $scope.checkConnect = function () {
      $scope.connect = ChekConnect.getConnectivite();
      $scope.testProfile();
      //console.log($scope.data.profile)
    }

    //Tester la connectiviteee
    $scope.checkConnect();

    $scope.getOptPays = function (option) {
      return option;
    };

    $scope.majvar = function () {
      $scope.data.nosubmit = true;
    }

    if ($scope.connect == false) {
      $scope.offline = angular.fromJson(localStorage.getItem('paysOnline'));
      $scope.listpayoffline = angular.fromJson(localStorage.getItem('paysOnline'));

      if ($scope.data.profile == "limite") {
        $scope.payschoisioffline = $scope.listpayoffline[0];
        console.log($scope.listpayoffline[0])
        $scope.listpayoffline = null;

        $scope.pointvente = []
        $scope.selectableNames = [];

        $scope.selectableNames = angular.fromJson(localStorage.getItem('pvOnline'))

        console.log($scope.selectableNames)
      } else {

      }

    }
    $scope.listpointdevnteoffline = function () {
      $scope.payschoisioffline = $scope.data.payschoisioffline;
      $scope.pointvente = []
      // console.log(angular.fromJson(localStorage.getItem('pvOnline')))
      $scope.pointvente = angular.fromJson(localStorage.getItem('pvOnline'))
      // console.log($scope.pointvente)
      $scope.selectableNames = [];
      if ($scope.pointvente != null) {
        for (var i = 0; i < $scope.pointvente.length; i++) {
          if ($scope.pointvente[i].codepays == $scope.payschoisioffline.code) {
            $scope.selectableNames.push($scope.pointvente[i]);
          }
          //var pv = { name: $scope.pointvente[i].client, id: $scope.pointvente[i].idpointVentes, code: $scope.pointvente[i].codePointVente, latitude: $scope.pointvente[i].latitude, longitude: $scope.pointvente[i].longitude }
        }
      }
      console.log($scope.selectableNames)
    }
    $scope.listpays = function () {
      // $scope.data.profil = ProfilUser.profilUser();

      var pays;
      var listdespays;
      var payschoisit;
      if (window.cordova) {
        if (navigator.connection.type == Connection.NONE) {
          connect = false;
        } else {
          connect = true;
          $scope.chargementDesPays();
        }

      } else {
        connect = true;
        $scope.chargementDesPays();
      }

    }
    $scope.listzonesSuper = function (idpays) {
      //$scope.listZones =
      ApiListZonesSuper
        .getListZone(idpays).success(response => {
          console.log(response)
          if (response) {
            $scope.data.listZones = response;

          }
        });
    }
    $scope.intPventAndZones = function () {
      $scope.listpointdevnte();
      $scope.listzonesSuper($scope.data.payschoisit.id);
    }
    $scope.chargementDesPays = function () {
      if ($scope.data.profile == 'super') {
        var url = urlPhp.getUrl();
        $http.get(url + "/pays.php")
          .success(function (response) {

            console.log(response)
            pays = response;

            $scope.data.listpays = [];
            for (var i = 0; i < response.length; i++) {
              var pv = { name: response[i].pays, id: response[i].idpays, code: response[i].code }
              $scope.data.listpays.push(pv);
            }
            console.log($scope.data.listpays)
            localStorage.setItem('paysOnline', angular.toJson($scope.data.listpays));
          }).catch(function (error) {

            console.log(error)
          });
        //
      } else {
        //Recuperer la liste des pays
        var url = urlPhp.getUrl();
        $http.get(url + "/paysByUser.php?idutilisateurs=" + sessionStorage.getItem('loggedin_iduser'))
          .success(function (response) {
            //  $ionicLoading.hide();
            pays = response;
            //  localStorage.setItem('paysOnline', angular.toJson(pays));
            $scope.data.listpays = [];
            console.log(response)
            for (var i = 0; i < response.length; i++) {
              var pv = { name: response[i].pays, id: response[i].idpays, code: response[i].code }
              $scope.data.listpays.push(pv);
            }
            if ($scope.data.listpays.length != 0) {
              //  payschoisit = $scope.data.listpays[0];
              $scope.data.payschoisit = $scope.data.listpays[0];
              $scope.intPventAndZones();
              localStorage.setItem('paysOnline', angular.toJson($scope.data.listpays));
              $scope.data.listpays = null;
            }

            console.log($scope.data.payschoisit)
            // $scope.listDesregionsByPaysID();
          }).catch(function (error) {
            // $ionicLoading.hide();
          });
        //Recuperer la liste des villes
      }
    }

    $scope.selectables = [
      1, 2, 3
    ];

    $scope.longList = [];
    for (var i = 0; i < 1000; i++) {
      $scope.longList.push(i);
    }

    $scope.getOpt = function (option) {
      //$scope.data.idpointVentes = option.id;
      return option;
    };
    $scope.getOptZone = function (option) {
      //$scope.data.idpointVentes = option.id;
      return option;
    };

    $scope.shoutLoud = function (newValuea, oldValue) {
      alert("changed from " + JSON.stringify(oldValue) + " to " + JSON.stringify(newValuea));
    };

    $scope.shoutReset = function () {
      alert("value was reset!");
    };

    var intervalGetPosition = null;

    $scope.jsonPositionsLog = [];
    $scope.isTrackingPosition = false;

    var latLng = new google.maps.LatLng(14.7645042, -17.3660286);

    var mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    $scope.map = new google.maps.Map(document.getElementById("mapf"), mapOptions);

    initGetLocationListener = function () {

      // init location listener
      intervalGetPosition = navigator.geolocation.watchPosition(function (position) {
        $scope.data.latitude = position.coords.latitude;
        $scope.data.logitude = position.coords.longitude;
        $scope.jsonPositionsLog.push({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        });

        $scope.$apply();
      },
        function (error) {
          console.log(error.message);
        }, {
        timeout: 3000
      });

    }

    var options = {
      timeout: 10000,
      enableHighAccuracy: true
    };

    calculDistance = function (maposition, pv) {
      var listDist = [];
      var f = function (a, b) {
        return a > b;
      }
      for (var i = 0; i < pv.length; i++) {
        var unit = 'K';
        var radlat1 = Math.PI * maposition.latitude / 180;
        var radlat2 = Math.PI * pv[i].latitude / 180;
        var theta = maposition.longitude - pv[i].longitude;
        var radtheta = Math.PI * theta / 180;
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        if (dist > 1) {
          dist = 1;
        }
        dist = Math.acos(dist);
        dist = dist * 180 / Math.PI;
        dist = dist * 60 * 1.1515;
        if (unit == "K") { dist = dist * 1.609344 }
        if (unit == "N") { dist = dist * 0.8684 }
        var p = { name: pv[i].name, id: pv[i].id, code: pv[i].code, latitude: pv[i].latitude, longitude: pv[i].longitude, distance: dist }
        listDist.push(p);
      }
      if (listDist.length != 0) {
        for (var i = 0; i < listDist.length; i++) {
          for (var j = i + 1; j < listDist.length; j++) {
            if (f(listDist[j].distance, listDist[i].distance)) {
              var temp = listDist[j];
              listDist[j] = listDist[i];
              listDist[i] = temp;
            }
          }
        }
      }

      return listDist;
    }
    $scope.listpays();
    $scope.listPvParZone = function () {

      if ($scope.data.listpointvente) {
        $scope.data.listpointvente.length = 0;
      }

      $ionicLoading.show({ content: 'Loading', animation: 'fade-in', showBackdrop: true, maxWidth: 200, showDelay: 0, duration: 10000 });
      ApiListPointventeParZone
        .pointventParZone($scope.data.payschoisit.id, $scope.data.zonechoisit.idzone)
        .success(function (response) {
          if (response) {
            $ionicLoading.hide();
            console.log(response)

            $scope.pointvente = angular.fromJson(response).sort();
            $ionicLoading.hide();

            $scope.selectableNames = [];
            $scope.list = [];
            $scope.lesplusproches = [];

            console.log($scope.pointvente)
            for (var i = 0; i < $scope.pointvente.length; i++) {
              var pv = {
                name: $scope.pointvente[i].pointVente,
                id: $scope.pointvente[i].idpointVentes,
                code: $scope.pointvente[i].codePointVente,
                latitude: $scope.pointvente[i].latitude,
                longitude: $scope.pointvente[i].longitude,
                codepays: $scope.data.payschoisit.code
              }
              $scope.selectableNames.push(pv);
            }
            //Chargement de point en local
            $scope.SauvegardeListPointVenteLocal($scope.selectableNames);
            //Calcul distance
            $cordovaGeolocation.getCurrentPosition(options).then(function (position) {
              var maposition = {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude
              }
              $scope.PointventeAuRayonDeuxKm(maposition, $scope.selectableNames);
            }, function (error) {

              console.log('Position error')
            })

          }
        })

    }
    $scope.SauvegardeListPointVenteLocal = function (list) {
      var pvlocal = angular.fromJson(localStorage.getItem('pvOnline'))
      var tempon = [];
      if (pvlocal == null || pvlocal.length == 0) {
        localStorage.setItem('pvOnline', angular.toJson(list));
      } else {
        pvlocal.concat(list);
        // pvlocal.push(...$scope.selectableNames)
        localStorage.setItem('pvOnline', angular.toJson(pvlocal));
      }
    }
    $scope.PointventeAuRayonDeuxKm = function (maposition, list) {
      $scope.list = calculDistance(maposition, list);
      if (list.length > 0) {
        $scope.lesplusproches = $scope.list.filter(item => item.distance <= 2)
        console.log($scope.lesplusproches);
        if ($scope.lesplusproches !== null && $scope.lesplusproches.length > 0) {
          $scope.pointVentePlusProche();
          $scope.markerPoinventSurMap(maposition);
        }
      }
    }
    $scope.pointVentePlusProche = function () {
      //Point vent plus proche
      if ($scope.lesplusproches == null || $scope.lesplusproches.length == 0) {

      } else {
        $translate('alert_header_point_detecter').then(function (header) {
          $translate('alert_button_oui').then(function (oui) {
            $translate('alert_button_non').then(function (non) {
              $ionicPopup.show({
                title: header,
                content: '{{ "alert_content_point_detecter" | translate }}',
                buttons: [
                  {
                    text: non,
                    type: 'button-assertive',
                    onTap: function (e) {
                      return false;
                    }
                  },
                  {
                    text: oui,
                    type: 'button-energized',
                    onTap: function (e) {
                      return true;
                    }
                  }]
              })
                .then(function (result) {
                  if (!result) {

                  } else {

                    $scope.data.pvchoisit = $scope.lesplusproches[$scope.lesplusproches.length - 1];
                  }
                })
            });
          });

        });

      }
    }
    $scope.listpointdevnte = function () {
      $scope.data.pvchoisit = null;

      var url = urlJava.getUrl();
      var link = url + "/yup/mespointVente"
      iduser = sessionStorage.getItem('loggedin_iduser')
      var user = {
        user: {
          "nom": "",
          "prenom": "",
          "telephone": "",
          "langue": "",
          "pays": "",
          "profil": "",
          "reseauxagent": "",
          "login": "",
          "password": "",
          "id": "" + iduser,
          "codePays": $scope.data.payschoisit.code
        }
      }
      //   console.log(user)
      $ionicLoading.show({ content: 'Loading', animation: 'fade-in', showBackdrop: true, maxWidth: 200, showDelay: 0, duration: 10000 });
      $http.post(link, user).then(function (res) {
        var options = {
          timeout: 10000,
          enableHighAccuracy: true
        };

        $scope.pointvente = angular.fromJson(res.data).sort();
        $ionicLoading.hide();
        $scope.selectableNames = [];
        $scope.list = [];
        $scope.lesplusproches = [];

        console.log($scope.pointvente)
        for (var i = 0; i < $scope.pointvente.length; i++) {
          var pv = {
            name: $scope.pointvente[i].pointvente,
            id: $scope.pointvente[i].id,
            code: $scope.pointvente[i].codePointVente,
            latitude: $scope.pointvente[i].latitude,
            longitude: $scope.pointvente[i].longitude,
            codepays: $scope.data.payschoisit.code
          }
          $scope.selectableNames.push(pv);
        }

        //Chargement de point en local
        $scope.SauvegardeListPointVenteLocal($scope.selectableNames);
        //Calcul distance
        $cordovaGeolocation.getCurrentPosition(options).then(function (position) {
          var maposition = {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
          }
          $scope.PointventeAuRayonDeuxKm(maposition, $scope.selectableNames);


        }, function (error) {
          $scope.pasDeLocation();
          console.log('Position error')
        })

      });

    }
    $scope.markerPoinventSurMap = function (maposition) {
      var latLng = new google.maps.LatLng(maposition.latitude, maposition.longitude);
      var mapOptions = {
        center: latLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      $scope.map = new google.maps.Map(document.getElementById("mapf"), mapOptions);


      //Wait until the map is loaded
      google.maps.event.addListenerOnce($scope.map, 'idle', function () {

        var marker = new google.maps.Marker({
          map: $scope.map,
          animation: google.maps.Animation.DROP,
          position: latLng,
          icon: 'img/marker.png'
        });

        var infoWindow = new google.maps.InfoWindow({
          content: "Ma position!"
        });

        google.maps.event.addListener(marker, 'click', function () {
          infoWindow.open($scope.map, marker);
        });

        $scope.lesplusproches.forEach(function (pv) {
          var marker = new google.maps.Marker({
            map: $scope.map,
            animation: google.maps.Animation.DROP,
            position: new google.maps.LatLng(pv.latitude, pv.longitude),
            icon: 'img/map-marker.png'
          });

          var infoWindow = new google.maps.InfoWindow({
            content: "Point: " + pv.name + "<br/>Code: " + pv.code + "<br/>Longitude: " + pv.longitude + "<br/>Latitude: " + pv.latitude
          });

          google.maps.event.addListener(marker, 'click', function () {
            infoWindow.open($scope.map, marker);
          })

        });

      })
    }
    $scope.pasDeLocation = function () {
      if (window.cordova) {

      } else {
        $scope.connect = true
      }
      if ($scope.connect == true) {
        $scope.oui = '';
        $scope.non = '';
        $translate('alert_button_oui').then(function (oui) {
          $scope.oui = oui;
          console.log(oui);
          $translate('alert_button_non').then(function (non) {
            $scope.non = non;
            //  console.log(non);

            $ionicPopup.show({
              title: '',
              content: '{{ "alert_content_position" | translate }}',
              buttons: [
                {
                  text: non,
                  type: 'button-assertive',
                  onTap: function (e) {
                    return false;
                  }
                },
                {
                  text: oui,
                  type: 'button-energized',
                  onTap: function (e) {
                    return true;
                  }
                }]
            })
              .then(function (result) {
                if (!result) {

                } else {
                  ionic.Platform.exitApp();
                }
              });
          });

        });


      }
    }
    $scope.startTracking = function () {
      // init location listener
      initGetLocationListener();
    }

    $scope.stopTrackingPosition = function () {
      navigator.geolocation.clearWatch(intervalGetPosition);
    }
    $scope.initvar = function () {
      $scope.data.bracheSecteur = null;
      $scope.data.brandingExterieur = null;
      $scope.data.connaissanceCodeEmployeTPE = null;
      $scope.data.dispositionTPE = null;
      $scope.data.flyers = null;
      $scope.data.formulaireClient = null;
      $scope.data.grilleTarifVisible = null;
      $scope.data.photoPoint = null;
      $scope.data.montantDeposit = null;
      $scope.data.niveauBatterieTPE = null;
      $scope.data.niveauFormation = null;
      $scope.data.presenceConcurence = null;
      $scope.data.brandingInterieur = null;
      $scope.data.idpointVentes = null;
      $scope.data.pvchoisit = null;
      $scope.data.pvchoisitoffline = null;
      $scope.data.actionAMener = null;
      $scope.data.latitude = null;
      $scope.data.longitude = null;
    }

    $scope.data = {};
    $scope.data.bracheSecteur = 'non';
    $scope.data.brandingExterieur = 'non';
    $scope.data.connaissanceCodeEmployeTPE = 'non';
    $scope.data.dispositionTPE = 'non';
    $scope.data.flyers = 'non';
    $scope.data.formulaireClient = 'non';
    $scope.data.grilleTarifVisible = 'non';
    $scope.data.photoPoint = 'non';
    $scope.data.montantDeposit = null;
    $scope.data.niveauBatterieTPE = '';
    $scope.data.niveauFormation = '';
    $scope.data.presenceConcurence = '';
    $scope.data.brandingInterieur = 'non';
    $scope.data.idpointVentes = "";
    $scope.date = new Date();
    $scope.data.reponse = "oui"
    // Enregistrement de la fiche de visite
    //Gerer les donnees locales: Synchronisation
    $scope.synchronDataLocal = function () {
      var flocal = angular.fromJson(localStorage.getItem('ficheSauvegarde'));
      if (flocal.length == 0) {
        console.log('Pas de donnees locales')
        $ionicLoading.hide();
      } else {
        $translate('header_title_synchrone').then(function (header) {
          $translate('content_label_synchrone').then(function (content) {
            $translate('content_label_synchrone_nb_fiche').then(function (nb) {
              $translate('content_label_synchrone_nb_suggetion').then(function (sugest) {
                var echec = [];
                $ionicPopup.show({
                  title: header,
                  template: content + "<br/>" + nb + flocal.length + "<br/>" + sugest,
                  scope: $scope,
                  buttons: [
                    {
                      text: 'OK',
                      type: 'button-energized',
                      onTap: function (e) {
                        return true;
                      }
                    }]
                }).then(function (result) {
                  if (!result) {

                  } else {
                    // console.log(flocal[0])
                    var url = urlJava.getUrl();
                    $ionicLoading.show({ content: 'Tentative de synchronisation', animation: 'fade-in', showBackdrop: true, maxWidth: 800, showDelay: 0, timeout: 10000 });
                    for (var i = 0; i < flocal.length; i++) {
                      $scope.data.value = flocal[i];
                      $http.post(url + '/yup/saveFiche', $scope.data.value).then(function (res) {
                        $ionicLoading.hide();
                      }).catch(function (error) {
                        echec.push(flocal[i])
                        $ionicLoading.hide();
                        alert('Synchro echouée');
                      });
                    }
                    if (echec.length == 0) {
                      var init = [];

                      $translate('alert_header_reussi').then(function (header) {
                        $translate('alert_content_reussi').then(function (content) {
                          $ionicPopup.confirm({
                            title: header,
                            content: content,
                            buttons: [
                              {
                                text: 'OK',
                                type: 'button-energized',
                                onTap: function (e) {
                                  return true;
                                }
                              }]
                          })
                            .then(function (result) {
                              if (result) {
                                localStorage.setItem('ficheSauvegarde', angular.toJson(init))
                              } else {
                                localStorage.setItem('ficheSauvegarde', angular.toJson(init))
                              }
                            });
                        })
                      })
                    } else {
                      localStorage.setItem('ficheSauvegarde', angular.toJson(echec))
                    }
                  }
                })

              });

            })
          })
        })


      }
    }
    $scope.validerForm = function () {
      console.log('control form')
      //control fromulaire
      var vide = false;
      if ($scope.data.montantDeposit == 0 ||
        $scope.data.montantDeposit == null ||
        $scope.data.niveauBatterieTPE == '' ||
        $scope.data.niveauFormation == '' ||
        $scope.data.presenceConcurence == '' ||
        $scope.data.pvchoisit.id == 0 ||
        $scope.data.longitude == null ||
        $scope.data.latitude == null
      ) {
        vide = true;
      }
      return vide;
    }
    $scope.alertForm = function () {
      $translate('alert_header_formulairvide').then(function (header) {
        $ionicPopup.show({
          title: header,
          template: '{{ "alert_content_formulairvide" | translate }}',
          scope: $scope,
          buttons: [
            {
              text: 'Ok',
              type: 'button-positive'
            }
          ]
        });
      });
    }
    $scope.matchingCheckBoxForm = function () {
      if ($scope.data.bracheSecteur == true) {
        $scope.data.bracheSecteur = 'OUI'
      } else {
        $scope.data.bracheSecteur = 'NON'
      }
      if ($scope.data.brandingExterieur == true) {
        $scope.data.brandingExterieur = 'OUI'
      } else {
        $scope.data.brandingExterieur = 'NON'
      }
      if ($scope.data.brandingInterieur == true) {
        $scope.data.brandingInterieur = 'OUI'
      } else {
        $scope.data.brandingInterieur = 'NON'
      }
      if ($scope.data.connaissanceCodeEmployeTPE == true) {
        $scope.data.connaissanceCodeEmployeTPE = 'OUI'
      } else {
        $scope.data.connaissanceCodeEmployeTPE = 'NON'
      }
      if ($scope.data.dispositionTPE == true) {
        $scope.data.dispositionTPE = 'OUI'
      } else {
        $scope.data.dispositionTPE = 'NON'
      }
      if ($scope.data.flyers == true) {
        $scope.data.flyers = 'OUI'
      } else {
        $scope.data.flyers = 'NON'
      }
      if ($scope.data.formulaireClient == true) {
        $scope.data.formulaireClient = 'OUI'
      } else {
        $scope.data.formulaireClient = 'NON'
      }
      if ($scope.data.grilleTarifVisible == true) {
        $scope.data.grilleTarifVisible = 'OUI'
      } else {
        $scope.data.grilleTarifVisible = 'NON'
      }
      if ($scope.data.photoPoint == true) {
        $scope.data.photoPoint = 'OUI'
      } else {
        $scope.data.photoPoint = 'NON'
      }

    }
    $scope.prepareObjetFivisite = function () {
      var value;
      value = {
        "fichev": {
          "actionAMener": $scope.data.actionAMener,
          "bracheSecteur": $scope.data.bracheSecteur,
          "brandingExterieur": $scope.data.brandingExterieur,
          "brandingInterieur": $scope.data.brandingInterieur,
          "connaissanceCodeEmployeTPE": $scope.data.connaissanceCodeEmployeTPE,
          "dispositionTPE": $scope.data.dispositionTPE,
          "flyers": $scope.data.flyers,
          "formulaireClient": $scope.data.formulaireClient,
          "grilleTarifVisible": $scope.data.grilleTarifVisible,
          "montantDeposit": $scope.data.montantDeposit,
          "niveauBatterieTPE": $scope.data.niveauBatterieTPE,
          "niveauFormation": $scope.data.niveauFormation,
          "photoPoint": $scope.data.photoPoint,
          "presenceConcurence": $scope.data.presenceConcurence,
          "idUser": sessionStorage.getItem('loggedin_iduser'),
          "idPointvent": $scope.data.pvchoisit.id,
          "clientPointvente": $scope.data.pvchoisit.code,
          "latitude": $scope.data.latitude,
          "longitude": $scope.data.longitude,
          "client": $scope.data.pvchoisit.name,
          "dateAjout": $scope.date,
          "reponse": $scope.data.reponse
        }
      };
      return value;
    }
    $scope.submit = function () {
      $scope.traitementLunch = true
      if ($scope.data.dblclk == 0) {
        $scope.date1 = new Date();
        console.log($scope.data);
        //initialiser coordonnees si connection =  false
        if (window.Connection) {
          if (navigator.connection.type == Connection.NONE) {
            if ($scope.data.longitude == null) {
              $scope.data.longitude = 0
            }
            if ($scope.data.latitude == null) {
              $scope.data.latitude = 0
            }
          }
        }
        //control fromulaire
        if ($scope.validerForm()) {
          $scope.traitementLunch = true
          $scope.alertForm();
          $scope.traitementLunch = false
        } else {

          $scope.matchingCheckBoxForm();
          $scope.data.value = $scope.prepareObjetFivisite();
          console.log($scope.data.value);


          if (!ChekConnect.getConnectivite()) {
            //insertion en local pour renvoi en cas de connection =true
            $translate('alert_header_ofline').then(function (header) {
              $translate('alert_content_ofline').then(function (content) {
                $translate('alert_button_oui').then(function (oui) {
                  $translate('alert_button_non').then(function (non) {

                    $ionicPopup.show({
                      title: header,
                      content: content,
                      buttons: [
                        {
                          text: non,
                          type: 'button-assertive',
                          onTap: function (e) {
                            return false;
                          }
                        },
                        {
                          text: oui,
                          type: 'button-energized',
                          onTap: function (e) {
                            return true;
                          }
                        }]
                    })
                      .then(function (result) {
                        if (!result) {
                          $scope.traitementLunch = false
                          ionic.Platform.exitApp();

                        } else {
                          var flocal = angular.fromJson(localStorage.getItem('ficheSauvegarde'));
                          console.log()
                          flocal.push($scope.data.value)
                          console.log(flocal)
                          localStorage.setItem('ficheSauvegarde', angular.toJson(flocal));
                          $translate('alert_header_reussi').then(function (header) {
                            $translate('alert_content_reussi').then(function (content) {
                              $ionicPopup.confirm({
                                title: header,
                                content: content,
                                buttons: [
                                  {
                                    text: 'OK',
                                    type: 'button-energized',
                                    onTap: function (e) {
                                      return true;
                                    }
                                  }]
                              })
                                .then(function (result) {
                                  if (result) {
                                    $scope.data.dblclk = 0;
                                    $scope.traitementLunch = false
                                    $scope.data = {};
                                    //  console.log(angular.fromJson(localStorage.getItem('ficheSauvegarde')))
                                  } else {

                                    $scope.data.dblclk = 0;
                                    $scope.data = {};
                                    $scope.traitementLunch = false
                                  }
                                });
                            });
                          });

                        }
                      });
                  });
                });


              });
            });

          } else {
            var url = urlJava.getUrl();
            $ionicLoading.show({ content: 'Loading', animation: 'fade-in', showBackdrop: true, maxWidth: 200, showDelay: 0, duration: 10000 });
            $http.post(url + '/yup/saveFiche', $scope.data.value).then(function (res) {
              $ionicLoading.hide();
              console.log(res)
              if (res.data[0].reponse == 'ok') {
                $translate('alert_header_reussi').then(function (header) {
                  $translate('alert_content_reussi').then(function (content) {
                    $ionicPopup.confirm({
                      title: header,
                      content: content,
                      buttons: [
                        {
                          text: 'OK',
                          type: 'button-energized',
                          onTap: function (e) {
                            return true;
                          }
                        }]
                    })
                      .then(function (result) {
                        if (!result) {
                          //forcer la syncronisation
                          $scope.traitementLunch = false
                          $scope.synchronDataLocal();
                        } else {
                          //forcer la syncronisation
                          $scope.traitementLunch = false
                          $scope.synchronDataLocal();
                        }
                      });
                  });

                });
                $scope.traitementLunch = false
                $scope.data.dblclk = 0;
                $scope.initvar();
              } else if (res.data[0].reponse == '20190416') {

                $translate('fichevisite_message_doublon').then(function (content) {
                  $translate('alert_button_oui').then(function (oui) {
                    $translate('alert_button_non').then(function (non) {
                      $ionicPopup.confirm({
                        title: " ",
                        content: content,
                        buttons: [
                          {
                            text: non,
                            type: 'button-assertive',
                            onTap: function (e) {
                              return false;
                            }
                          },
                          {
                            text: oui,
                            type: 'button-energized',
                            onTap: function (e) {
                              return true;
                            }
                          }]
                      })
                        .then(function (result) {
                          if (!result) {

                          } else {

                            $scope.data.value = {
                              "fichev": {
                                "actionAMener": res.data[0].actionAMener,
                                "bracheSecteur": res.data[0].bracheSecteur,
                                "brandingExterieur": res.data[0].brandingExterieur,
                                "brandingInterieur": res.data[0].brandingInterieur,
                                "connaissanceCodeEmployeTPE": res.data[0].connaissanceCodeEmployeTPE,
                                "dispositionTPE": res.data[0].dispositionTPE,
                                "flyers": res.data[0].flyers,
                                "formulaireClient": res.data[0].formulaireClient,
                                "grilleTarifVisible": res.data[0].grilleTarifVisible,
                                "montantDeposit": $scope.data.montantDeposit,
                                "niveauBatterieTPE": $scope.data.niveauBatterieTPE,
                                "niveauFormation": res.data[0].niveauFormation,
                                "photoPoint": res.data[0].photoPoint,
                                "presenceConcurence": res.data[0].presenceConcurence,
                                "idUser": res.data[0].idUser,
                                "idPointvent": res.data[0].idPointvent,
                                "clientPointvente": res.data[0].clientPointvente,
                                "latitude": res.data[0].latitude,
                                "longitude": res.data[0].longitude,
                                "client": res.data[0].clientPointvente,
                                "dateAjout": res.data[0].dateAjout,
                                "reponse": "confirme"
                              }
                            };
                            console.log($scope.data.value)
                            var url = urlJava.getUrl();
                            $ionicLoading.show({ content: 'Loading', animation: 'fade-in', showBackdrop: true, maxWidth: 200, showDelay: 0, duration: 10000 });
                            $http.post(url + '/yup/saveFiche', $scope.data.value).then(function (res) {

                              $ionicLoading.hide();
                              console.log(res)

                              $translate('alert_header_reussi').then(function (header) {
                                $translate('alert_content_reussi').then(function (content) {
                                  $ionicPopup.confirm({
                                    title: header,
                                    content: content,
                                    buttons: [
                                      {
                                        text: 'OK',
                                        type: 'button-energized',
                                        onTap: function (e) {
                                          return true;
                                        }
                                      }]
                                  })
                                    .then(function (result) {
                                      if (!result) {

                                      } else {

                                      }
                                    });
                                });

                              });
                              $scope.traitementLunch = false
                              $scope.data.dblclk = 0;
                              $scope.initvar();
                            }).catch(function (error) {
                              $ionicLoading.hide();
                              $scope.data.dblclk = 0;
                              $scope.traitementLunch = false
                              $translate('alert_insert_echec').then(function (content) {
                                alert(content);
                              });

                            });

                          }
                        });
                    })
                  })

                });

                $scope.traitementLunch = false
                $scope.data.dblclk = 0;
              }


            }).catch(function (error) {
              $ionicLoading.hide();
              $scope.data.dblclk = 0;
              $scope.traitementLunch = false
              $translate('alert_insert_echec').then(function (content) {
                alert(content);
              });

            });
          }


        }

      } else {
        $translate('alert_ddlclck').then(function (content) {
          $translate('alert_header_formulairvide').then(function (header) {
            $ionicPopup.show({
              title: header,
              template: content,
              scope: $scope,
              buttons: [{
                text: 'Ok',
                type: 'button-positive'
              }]
            }).then(function (result) {
              if (!result) {
                $scope.data.dblclk = 0;
                $scope.traitementLunch = false

              } else {
                $scope.data.dblclk = 0;
                $scope.traitementLunch = false
              }
            });
          });


        });

      }
    }
  })
  .controller('ProspectsCtrl', function ($scope,
    $http,
    $ionicLoading,
    $ionicPopup,
    $cordovaGeolocation,
    ChekConnect,
    $translate,
    $cordovaCamera,
    $ionicModal,
    ProfilUser,
    urlPhp,
    urlJava) {

    $scope.data = {};
    $scope.data.regionchoisit = null;
    $scope.data.villechoisit = null;
    $scope.data.payschoisit = null;
    $scope.connect = null;
    $scope.data.date = new Date();
    $scope.data.longitude = '0';
    $scope.data.latitude = '0';
    $scope.data.profile = 'limite';
    $scope.images = [];
    $scope.img = '';
    $scope.showphoto = true;


    $scope.testProfile = function () {

      if (localStorage.getItem('loggedin_profil') == 'Codir YUP Mgt' || localStorage.getItem('loggedin_profil') == 'Direction Commerciale YUP Mgt'
        || localStorage.getItem('loggedin_profil') == 'Marketing YUP Mgt' || localStorage.getItem('loggedin_profil') == 'Call Center YUP Mgt'
        || localStorage.getItem('loggedin_profil') == 'Administrateur Maintenance') {
        // $scope.data.profile = 'super';
        $scope.data.profile = 'super';
      }
      // return profil;
    }
    // $scope.data.profile = ProfilUser.profilUser();

    //Tester la connectiviteee
    $scope.checkConnect = function () {
      $scope.testProfile();
      $scope.connect = ChekConnect.getConnectivite();
    }
    $scope.initCtrl = function () {
      $scope.checkConnect();
      if ($scope.connect == false) {
        $scope.showphoto = false;
        $translate('alert_header_ofline').then(function (header) {
          $translate('alert_content_ofline_list').then(function (content) {
            $translate('alert_button_oui').then(function (oui) {
              $translate('alert_button_non').then(function (non) {
                $ionicPopup.show({
                  title: header,
                  content: content,
                  buttons: [
                    {
                      text: non,
                      type: 'button-assertive',
                      onTap: function (e) {
                        return false;
                      }
                    },
                    {
                      text: oui,
                      type: 'button-energized',
                      onTap: function (e) {
                        return true;
                      }
                    }]
                })
                  .then(function (result) {
                    if (!result) {
                      ionic.Platform.exitApp();
                    }
                  });
              });
            });
          });
        });

      }
    }
    //Tester la connectiviteee
    $scope.initCtrl();
    //Initialiser la liste de regions selon le connectivite
    $scope.initReg = function () {
      if ($scope.connect == true) {

        $scope.chargerLesRegionsEnLocal();
        $scope.chargerLesVillesEnLocal();

        $ionicLoading.show({
          content: 'Loading',
          animation: 'fade-in',
          showBackdrop: true,
          maxWidth: 200,
          showDelay: 0,
          duration: 10000
        });
        if ($scope.data.profile == 'super') {
          var url = urlPhp.getUrl();
          $http.get(url + "/pays.php")
            .success(function (response) {
              $ionicLoading.hide();
              $scope.pays = response;
              localStorage.setItem('paysOnline', angular.toJson($scope.pays));
              $scope.listdespays = [];
              for (var i = 0; i < response.length; i++) {
                var pv = { name: response[i].pays, id: response[i].idpays }
                $scope.listdespays.push(pv);
              }
            }).catch(function (error) {
              $ionicLoading.hide();
            });
          //

        } else {
          //Recuperer la liste des pays
          var url = urlPhp.getUrl();
          $http.get(url + "/paysByUser.php?idutilisateurs=" + sessionStorage.getItem('loggedin_iduser'))
            .success(function (response) {
              $ionicLoading.hide();
              $scope.pays = response;
              localStorage.setItem('paysOnline', angular.toJson($scope.pays));

              $scope.listdespays = [];
              for (var i = 0; i < response.length; i++) {
                var pv = { name: response[i].pays, id: response[i].idpays }
                $scope.listdespays.push(pv);
              }
              if ($scope.listdespays.length != 0) {
                $scope.data.payschoisit = $scope.listdespays[0];
              }
              $scope.listDesregionsByPaysID();
            }).catch(function (error) {
              $ionicLoading.hide();

            });
          //Recuperer la liste des villes

        }


      } else {
        //console.log('eerror connexion')
        $scope.pays = []
        $scope.pays = angular.fromJson(localStorage.getItem('paysOnline'))
        // console.log($scope.pointvente)
        $scope.listdespays = [];
        if ($scope.pays != null) {
          for (var i = 0; i < $scope.pays.length; i++) {
            var pv = { name: $scope.pays[i].pays, id: $scope.pays[i].idpays }
            $scope.listdespays.push(pv);
          }
        }
        if ($scope.data.profile == 'limite') {
          $scope.data.payschoisit = $scope.listdespays[0]
        }
        $scope.listDesregionsByPaysID();
      }
    }

    $scope.listDesregionsByPaysID = function () {

      if ($scope.connect == true) {
        //Recuperer la liste des regions
        var url = urlPhp.getUrl();
        $http.get(url + "/regionsByPays.php?idpays=" + $scope.data.payschoisit.id)
          .success(function (response) {
            $ionicLoading.hide();
            $scope.region = response;
            //  localStorage.setItem('regionsOnline', angular.toJson($scope.region));
            $scope.listregions = [];
            for (var i = 0; i < response.length; i++) {
              var pv = { name: response[i].region, id: response[i].idregion }
              $scope.listregions.push(pv);
            }

          }).catch(function (error) {
            $ionicLoading.hide();

          });
      } else {
        $scope.region = []
        $scope.region = angular.fromJson(localStorage.getItem('regionsOnline'))
        // console.log($scope.pointvente)
        $scope.listregions = [];
        if ($scope.data.profile == 'super') {
          //   $scope.listregions =  $scope.region;
          for (var i = 0; i < $scope.region.length; i++) {

            var pv = { name: $scope.region[i].region, id: $scope.region[i].idregion }
            $scope.listregions.push(pv);


          }
        } else {

          if ($scope.region != null) {
            for (var i = 0; i < $scope.region.length; i++) {
              if ($scope.region[i].idpays == $scope.data.payschoisit.id) {
                var pv = { name: $scope.region[i].region, id: $scope.region[i].idregion }
                $scope.listregions.push(pv);
              }

            }
          }
        }
      }

    }
    $scope.initvar = function () {
      $scope.data.adresse = null
      $scope.data.telephone = null
      $scope.data.frequentationdeslieux = null
      $scope.data.prospect = null
      $scope.data.tailledupoint = null
      $scope.data.presencedelaconcurrence = null
      $scope.data.InteretmanifestepourYUP = null
      $scope.data.frequentationdeslieux = null
      $scope.data.latitude = null
      $scope.data.longitude = null
      $scope.data.regionchoisit = null
      $scope.data.villechoisit = null
      $scope.data.commentaire = null
      $scope.data.telephone = null
      $scope.img = null
      $scope.images = null
      $scope.data.date = new Date();
    }
    $scope.listVillesByRegionID = function () {
      if ($scope.connect == true) {
        //Recuperer la liste des villes
        var url = urlPhp.getUrl();
        $http.get(url + "/villeByRegion.php?idregion=" + $scope.data.regionchoisit.id)
          .success(function (response) {
            $ionicLoading.hide();
            $scope.ville = response;
            // localStorage.setItem('villesOnline', angular.toJson($scope.ville));
            $scope.listvilles = [];
            for (var i = 0; i < response.length; i++) {
              var pv = { name: response[i].ville, id: response[i].idville }
              $scope.listvilles.push(pv);
            }
            //    console.log($scope.listvilles)
          }).catch(function (error) {
            $ionicLoading.hide();
          });
      } else {
        $scope.ville = []
        $scope.ville = angular.fromJson(localStorage.getItem('villesOnline'))
        // console.log($scope.pointvente)
        $scope.listvilles = [];
        if ($scope.ville != null) {
          for (var i = 0; i < $scope.ville.length; i++) {
            if ($scope.ville[i].idregion == $scope.data.regionchoisit.id) {
              var pv = { name: $scope.ville[i].ville, id: $scope.ville[i].idville }
              $scope.listvilles.push(pv);
            }

          }
        }
      }

    }
    $scope.chargerLesVillesEnLocal = function () {
      $ionicLoading.show({
        content: 'Loading',
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0,
        duration: 10000
      });
      //Recuperer la liste des villes
      var url = urlPhp.getUrl();
      $http.get(url + "/Allville.php")
        .success(function (response) {
          $ionicLoading.hide();
          $scope.ville = response;
          localStorage.setItem('villesOnline', angular.toJson($scope.ville));
        }).catch(function (error) {
          $ionicLoading.hide();
        });
    }
    $scope.chargerLesRegionsEnLocal = function () {
      $ionicLoading.show({
        content: 'Loading',
        animation: 'fade-in',
        showBackdrop: true,
        maxWidth: 200,
        showDelay: 0,
        duration: 10000
      });
      //Recuperer la liste des regions
      var url = urlPhp.getUrl();
      $http.get(url + "/Allregions.php")
        .success(function (response) {
          $ionicLoading.hide();
          $scope.region = response;
          console.log(response);
          localStorage.setItem('regionsOnline', angular.toJson($scope.region));
        }).catch(function (error) {
          $ionicLoading.hide();

        });
    }
    //Initialiser la liste de regions selon le connectivite
    $scope.initReg();
    //Gerer les donnees locales: Synchronisation
    $scope.synchronDataLocalProspect = function () {
      var flocal = angular.fromJson(localStorage.getItem('prospectsSauvegarde'));
      if (flocal.length == 0) {
        console.log('Pas de donnees locales')

      } else {
        $translate('header_title_synchrone').then(function (header) {
          $translate('content_label_synchrone').then(function (content) {
            $translate('content_label_synchrone_nb_fiche').then(function (nb) {
              var echec = [];
              $ionicPopup.show({
                title: header,
                template: content + "<br/>" + nb + flocal.length,
                scope: $scope,
                buttons: [
                  {
                    text: 'OK',
                    type: 'button-energized',
                    onTap: function (e) {
                      return true;
                    }
                  }]
              })
                .then(function (result) {
                  if (!result) {

                  } else {
                    var url = urlPhp.getUrl();
                    $ionicLoading.show({ content: 'Tentative de synchronisation', animation: 'fade-in', showBackdrop: true, maxWidth: 800, showDelay: 0, timeout: 10000 });
                    for (var i = 0; i < flocal.length; i++) {
                      $http.post(url + '/prospect.php', flocal[i]).then(function (res) {
                        console.log('index :' + i + " " + res.data)
                      }).catch(function (error) {
                        echec.push(flocal[i])
                        console.log('echoue')
                        $ionicLoading.hide();
                        alert('Synchro echouée');
                      });
                    }
                    console.log('Taille apres la boucle')
                    console.log(echec)
                    $ionicLoading.hide();

                    if (echec.length == 0) {
                      var init = [];
                      console.log('tout est envoyé')
                      $translate('alert_header_reussi').then(function (header) {
                        $translate('alert_content_reussi').then(function (content) {
                          $ionicPopup.confirm({
                            title: header,
                            content: content,
                            buttons: [
                              {
                                text: 'OK',
                                type: 'button-energized',
                                onTap: function (e) {
                                  return true;
                                }
                              }]
                          })
                            .then(function (result) {
                              if (result) {
                                localStorage.setItem('prospectsSauvegarde', angular.toJson(init))
                              } else {
                                localStorage.setItem('prospectsSauvegarde', angular.toJson(init))
                              }
                            });
                        })
                      })


                    } else {
                      console.log('Taille s il ya erreur')
                      console.log(echec)
                      localStorage.setItem('prospectsSauvegarde', angular.toJson(echec))
                    }
                  }
                });

            })

          })
        })


      }
    }
    $scope.selectables = [
      1, 2, 3
    ];
    $scope.longList = [];
    for (var i = 0; i < 1000; i++) {
      $scope.longList.push(i);
    }

    $scope.getOptPays = function (option) {

      return option;
    };
    $scope.getOptRegion = function (option) {
      //   console.log($scope.data.regionchoisit)
      return option;
    };
    $scope.getOptVille = function (option) {
      //   console.log($scope.data.villechoisit)
      return option;
    };

    $scope.shoutLoud = function (newValuea, oldValue) {
      alert("changed from " + JSON.stringify(oldValue) + " to " + JSON.stringify(newValuea));
    };

    $scope.shoutReset = function () {
      alert("value was reset!");
    };


    var intervalGetPosition;

    $scope.jsonPositionsLog = [];
    $scope.isTrackingPosition = false;

    $scope.startTracking = function () {
      // init location listener
      initGetLocationListener();
    }

    $scope.stopTrackingPosition = function () {
      navigator.geolocation.clearWatch(intervalGetPosition);
    }

    getCurrentPosition = function () {
      navigator.geolocation.getCurrentPosition(function (position) {
        // get lat and long
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;

        // add positions to array
        $scope.jsonPositionsLog.push({
          latitude: latitude,
          longitude: longitude
        });

        $scope.$apply();
      });
    }

    initGetLocationListener = function () {
      // init location listener
      intervalGetPosition = navigator.geolocation.watchPosition(function (position) {
        $scope.jsonPositionsLog.push({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        });
        $scope.$apply();
      },
        function (error) {
          console.log(error.message);
        }, {
        timeout: 3000
      });
    }

    var options = {
      timeout: 10000,
      enableHighAccuracy: true
    };

    $cordovaGeolocation.getCurrentPosition(options).then(function (position) {

      var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

      var mapOptions = {
        center: latLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      $scope.map = new google.maps.Map(document.getElementById("mapp"), mapOptions);


      //Wait until the map is loaded
      google.maps.event.addListenerOnce($scope.map, 'idle', function () {

        var marker = new google.maps.Marker({
          map: $scope.map,
          animation: google.maps.Animation.DROP,
          position: latLng
        });

        var infoWindow = new google.maps.InfoWindow({
          content: "Je suis ici !"
        });

        google.maps.event.addListener(marker, 'click', function () {
          infoWindow.open($scope.map, marker);
        });

      });

    }, function (error) {
      console.log("Could not get location");
    });

    localStorage.getItem("username");
    localStorage.getItem("password");

    console.log(localStorage.getItem("username"));
    console.log(localStorage.getItem("password"));


    $ionicModal.fromTemplateUrl('modal.html', function (modal) {
      $scope.gridModal = modal;
    }, {
      scope: $scope,
      animation: 'slide-in-up'
    })
    $scope.openModal = function (data) {
      $scope.inspectionItem = data;
      $scope.gridModal.show();
    }
    $scope.closeModal = function () {
      $scope.gridModal.hide();
    }


    $scope.addImage = function () {
      // 2
      $scope.images = null;
      $ionicLoading.show({
        template: 'Chargement...'
      });
      var options = {
        quality: 50,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.CAMERA,
        allowEdit: false,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 500,
        targetHeight: 500,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false,
        correctOrientation: true
      };

      // 3
      $cordovaCamera.getPicture(options).then(function (imageData) {
        // 4
        //onImageSuccess(imageData);
        $scope.images = "data:image/jpeg;base64," + imageData;
        $scope.img = imageData;
        $ionicLoading.hide();
      }, function (err) {
        $ionicLoading.hide();
        console.log(err);
      });
    }
    $scope.insetImage = function (fiche) {
      var url = urlJava.getUrl();
      var link = url + "/mediatheque/uploadImageProspect";
      $http.post(link, {
        "idfiche": fiche,
        "imageData": "" + $scope.img
      }).then(function (res) {
        $translate('alert_header_reussi').then(function (header) {
          $translate('alert_content_reussi').then(function (content) {
            $ionicPopup.show({
              title: header,
              template: content,
              scope: $scope,
              buttons: [
                {
                  text: 'OK',
                  type: 'button-energized',
                  onTap: function (e) {
                    return true;
                  }
                }]
            })
              .then(function (result) {
                if (!result) {
                  //forcer la syncronisation
                  $scope.synchronDataLocalProspect();
                } else {
                  //forcer la syncronisation
                  $scope.synchronDataLocalProspect();
                }
              });
          });
        });
        $scope.initvar();
      }).catch(function (error) {
        console.log(error)
        alert(error);
      });/*.cath(function (err) {
        alert('Error');
      });*/
    }
    $scope.submit = function () {
      $scope.checkConnect();

      if ($scope.data.adresse == null ||
        $scope.data.frequentationdeslieux == null ||
        $scope.data.prospect == null ||
        $scope.data.tailledupoint == null ||
        $scope.data.presencedelaconcurrence == null ||
        $scope.data.InteretmanifestepourYUP == null ||
        $scope.data.frequentationdeslieux == null ||
        $scope.data.telephone == null) {
        $translate('alert_header_formulairvide').then(function (header) {
          $translate('alert_content_formulairvide').then(function (content) {
            $ionicPopup.show({
              title: header,
              template: content,
              scope: $scope,
              buttons: [{
                text: 'Ok',
                type: 'button-positive'
              }]
            });
          });
        });

      } else {

        $scope.value = {
          prospect: $scope.data.prospect,
          idregions: $scope.data.villechoisit.id,
          commentaire: $scope.data.commentaire,
          longitude: $scope.data.longitude,
          latitude: $scope.data.latitude,
          adresse: $scope.data.adresse,
          frequantationdeslieux: "" + $scope.data.frequentationdeslieux,
          tailledupoint: $scope.data.tailledupoint,
          presencedelaconcurrence: $scope.data.presencedelaconcurrence,
          InteretmanifestepourYUP: $scope.data.InteretmanifestepourYUP,
          idutilisateurs: sessionStorage.getItem('loggedin_iduser'),
          ville: $scope.data.villechoisit.id,
          dateajout: $scope.data.date,
          telephone: $scope.data.telephone
        };
        if ($scope.connect == true) {
          var url = urlPhp.getUrl();
          var link = url + '/prospect.php';
          //  $scope.data.latitude = 14.3274633;
          //  $scope.data.longitude = -17.324723845742;
          $ionicLoading.show({ content: 'Loading', animation: 'fade-in', showBackdrop: true, maxWidth: 200, showDelay: 0, duration: 10000 });
          console.log($scope.value)
          $http.post(link, $scope.value)
            .then(function (res) {
              console.log("Retour insert prospect")
              console.log(res)
              //Inserer l image
              if (res.data == "1") {
                $scope.insetImage(res.data)
              }

              $ionicLoading.hide();

            }).catch(function (error) {
              console.log(error)
              $ionicLoading.hide();
              alert(error);
            });
        } else {
          //insertion en local pour renvoi en cas de connection =true
          $translate('alert_header_ofline').then(function (header) {
            $translate('alert_content_ofline').then(function (content) {
              $translate('alert_button_oui').then(function (oui) {
                $translate('alert_button_non').then(function (non) {
                  $ionicPopup.show({
                    title: header,
                    content: content,
                    buttons: [
                      {
                        text: non,
                        type: 'button-assertive',
                        onTap: function (e) {
                          return false;
                        }
                      },
                      {
                        text: oui,
                        type: 'button-energized',
                        onTap: function (e) {
                          return true;
                        }
                      }]
                  })
                    .then(function (result) {
                      if (!result) {

                      } else {
                        $scope.value = {
                          prospect: $scope.data.prospect,
                          idregions: $scope.data.regionchoisit.id,
                          commentaire: $scope.data.commentaire,
                          longitude: '0',
                          latitude: '0',
                          adresse: $scope.data.adresse,
                          Frequentationdeslieux: "" + $scope.data.frequentationdeslieux,
                          tailledupoint: $scope.data.tailledupoint,
                          presencedelaconcurrence: $scope.data.presencedelaconcurrence,
                          InteretmanifestepourYUP: $scope.data.InteretmanifestepourYUP,
                          idutilisateurs: sessionStorage.getItem('loggedin_iduser'),
                          ville: $scope.data.regionchoisit.name,
                          dateajout: $scope.data.date
                        };
                        var plocal = angular.fromJson(localStorage.getItem('prospectsSauvegarde'));
                        plocal.push($scope.value)
                        localStorage.setItem('prospectsSauvegarde', angular.toJson(plocal));
                        $translate('alert_header_reussi').then(function (header) {
                          $translate('alert_content_reussi').then(function (content) {
                            $ionicPopup.confirm({
                              title: header,
                              content: content,
                              buttons: [
                                {
                                  text: 'OK',
                                  type: 'button-energized',
                                  onTap: function (e) {
                                    return true;
                                  }
                                }]
                            })
                              .then(function (result) {
                                if (result) {
                                  $scope.initvar();
                                } else {
                                  $scope.initvar();
                                }
                              });
                          });
                        });

                      }
                    });

                });
              });
            });
          });

        }
      }
    }
  })
  .controller('MesfichesCtrl', function ($scope, $http, $ionicLoading, $ionicPopup, $translate, urlJava) {
    $scope.data = {};
    $scope.checkConnect = function () {
      if (window.Connection) {
        console.log('in')
        if (navigator.connection.type == Connection.NONE) {
          $scope.data.connect = false;
          $translate('alert_header_ofline').then(function (header) {
            $translate('alert_content_ofline_list').then(function (content) {
              $translate('alert_button_oui').then(function (oui) {
                $translate('alert_button_non').then(function (non) {
                  $ionicPopup.show({
                    title: header,
                    content: content,
                    buttons: [
                      {
                        text: oui,
                        type: 'button-assertive',
                        onTap: function (e) {
                          return false;
                        }
                      },
                      {
                        text: non,
                        type: 'button-energized',
                        onTap: function (e) {
                          return true;
                        }
                      }]
                  })
                    .then(function (result) {
                      if (!result) {

                        $ionicLoading.show({ content: 'Loading', animation: 'fade-in', showBackdrop: true, maxWidth: 200, showDelay: 0, duration: 10000 });
                        var flocal = angular.fromJson(localStorage.getItem('ficheSauvegarde'));
                        console.log(flocal)
                        $scope.mesfichesvisitelocal = flocal;
                        $ionicLoading.hide();
                      } else {
                        ionic.Platform.exitApp();
                      }
                    });
                });
              });
            });
          });

        }
        else {
          $scope.data.connect = true;
          iduser = sessionStorage.getItem('loggedin_iduser')
          var user = {
            user: {
              "nom": "",
              "prenom": "",
              "telephone": "",
              "langue": "",
              "pays": "",
              "profil": "",
              "reseauxagent": "",
              "login": "",
              "password": "",
              "id": "" + iduser
            }
          }
          $ionicLoading.show({ content: 'Loading', animation: 'fade-in', showBackdrop: true, maxWidth: 200, showDelay: 0, duration: 3000 });
          var url = urlJava.getUrl();
          var link = url + "/yup/mesfichesdevisites";
          $http.post(link, user)
            .success(function (response) {
              $ionicLoading.hide();
              $scope.synchronDataLocal();
              console.log(response)
              $scope.mesfichesvisite = response;
              //console.log($scope.mesfichesvisite);
            }).catch(function (error) {
              $ionicLoading.hide();
              alert(error);
            });
        }
      }
    }
    $scope.checkConnect();
    //Gerer les donnees locales: Synchronisation
    $scope.synchronDataLocal = function () {
      // $ionicLoading.show({ content: 'Tentative de synchronisation', animation: 'fade-in', showBackdrop: true, maxWidth: 800, showDelay: 0, timeout: 10000 });

      var flocal = angular.fromJson(localStorage.getItem('ficheSauvegarde'));
      if (flocal.length == 0) {
        console.log('Pas de donnees locales')
        //   $ionicLoading.hide();
      } else {

        $translate('header_title_synchrone').then(function (header) {
          $translate('content_label_synchrone').then(function (content) {
            $translate('content_label_synchrone_nb_fiche').then(function (nb) {
              $translate('content_label_synchrone_nb_suggetion').then(function (sugest) {
                var echec = [];
                $ionicPopup.show({
                  title: header,
                  template: content + "<br/>" + nb + flocal.length + "<br/>" + sugest,
                  scope: $scope,
                  buttons: [
                    {
                      text: 'OK',
                      type: 'button-energized',
                      onTap: function (e) {
                        return true;
                      }
                    }]
                }).then(function (result) {
                  if (!result) {

                  } else {
                    // console.log(flocal[0])
                    $ionicLoading.show({ content: 'Tentative de synchronisation', animation: 'fade-in', showBackdrop: true, maxWidth: 800, showDelay: 0, timeout: 3000 });
                    var url = urlJava.getUrl();
                    for (var i = 0; i < flocal.length; i++) {
                      $scope.data.value = flocal[i];
                      $http.post(url + '/yup/saveFiche', $scope.data.value).then(function (res) {
                        $ionicLoading.hide();
                      }).catch(function (error) {
                        echec.push(flocal[i])
                        $ionicLoading.hide();
                        alert('Synchro echouée');
                      });
                    }
                    if (echec.length == 0) {
                      var init = [];

                      $translate('alert_header_reussi').then(function (header) {
                        $translate('alert_content_reussi').then(function (content) {
                          $ionicPopup.confirm({
                            title: header,
                            content: content,
                            buttons: [
                              {
                                text: 'OK',
                                type: 'button-energized',
                                onTap: function (e) {
                                  return true;
                                }
                              }]
                          })
                            .then(function (result) {
                              if (result) {
                                localStorage.setItem('ficheSauvegarde', angular.toJson(init))
                              } else {
                                localStorage.setItem('ficheSauvegarde', angular.toJson(init))
                              }
                            });
                        })
                      })
                    } else {
                      localStorage.setItem('ficheSauvegarde', angular.toJson(echec))
                    }
                  }
                })

              });

            })
          })
        })

      }
    }
  })

  .controller('SignupCtrl', function ($scope, $http, $ionicPopup, $state, $translate, urlPhp) {
    $scope.data = {};
    $scope.myusername = localStorage.getItem("username");
    //console.log($scope.myusername);

    $scope.submit = function () {
      var url = urlPhp.getUrl();
      var link = url + '/compte.php';

      $http.post(link, {
        loginame: $scope.data.username,
        newpassword: $scope.data.newpassword1,
        newpasswordc: $scope.data.newpassword2,
      });

      $http.get(url + '/compte.php?username=' + sessionStorage.getItem('loggedin_name') + '&newpassword=' + $scope.data.newpassword1 + '&newpasswordc=' + $scope.data.newpassword2).then(function (res) {
        $scope.response = res.data;
        //console.log($scope.response);
        $translate('alert_header_reussi').then(function (header) {
          $translate('alert_content_reussi').then(function (content) {

            $ionicPopup.show({
              title: header,
              template: content,
              // buttons: ['Ok'],
              scope: $scope,
              buttons: [
                {
                  text: 'OK',
                  type: 'button-energized',
                  onTap: function (e) {
                    return true;
                  }
                }]
            })
              .then(function (result) {
                if (result) {
                  if (res.data == "Changement effectué avec success !") {
                    $scope.data = {};
                    var initialHref = window.location.href;

                    function restartApplication() {
                      // Show splash screen (useful if your app takes time to load)
                      navigator.splashscreen.show();
                      // Reload original app url (ie your index.html file)
                      window.location = initialHref;
                    }
                    $state.transitionTo('app.accueil', {}, {
                      reload: true,
                      inherit: true,
                      notify: true
                    });
                  }

                } else {
                  $scope.initvar();
                }
              });

          });
        });


      });
    };
  })
  .controller('LogoutCtrl', function () {

    sessionStorage.clear();
    localStorage.clear();
    localStorage.setItem('loggedin_name', 'null');
    localStorage.setItem('loggedin_password', 'null');
    localStorage.getItem('loggedin_profil', null);

  })
.controller('ProfileCtrl', function ($scope, $http, $ionicPopup, $state, $translate, urlPhp) {
    $scope.data = {};
    $scope.myusername = localStorage.getItem("username");
    //console.log($scope.myusername);
    $scope.submit = function () {
      // var link = 'http://vps101245.ovh.net:84/webservice/compte.php';
      var url = urlPhp.getUrl();
      $http.get(url + '/password.php?username=' + localStorage.getItem("username")).then(function (res) {
        $scope.response = res.data;
        //console.log($scope.response);

        $translate('alert_header_reussi').then(function (header) {
          $translate('alert_content_reussi').then(function (content) {
            $ionicPopup.show({
              title: header,
              template: content,
              scope: $scope,
              buttons: [
                {
                  text: 'Ok',
                  type: 'button-positive'
                }
              ]
            });
          });
        });

        if (res.data == "Changement  success !") {
          $state.go('app.login1');
        }
      });
    };
  })

  .controller('FichesCtrl', function ($scope, $http, urlPhp) {
    var url = urlPhp.getUrl();
    $http.get(url + "/fiches.php")
      .success(function (response) {
        $scope.fichevisite = response;
      });
  })

  .controller('CompteCtrl', function ($state) {
    $state.go('app.compte');

  })

  .factory('Markers', function ($http, urlPhp) {

    var markers = [];

    return {
      getMarkers: function () {
        var url = urlPhp.getUrl();
        return $http.get(url + "/fiches.php?idutilisateurs=" + sessionStorage.getItem('loggedin_iduser')).then(function (response) {
          markers = response;
          return markers;
        });

      }
    }

  })
  .controller('MapCtrl', function ($scope, $cordovaGeolocation, $http, urlPhp, ChekConnect,
    $translate,
    ProfilUser,
    $ionicLoading,
    ChekConnect,
    $translate,
    ProfilUser,
    urlJava, ) {
    $scope.data.payschoisit = null
    $scope.pvtempon = [];
    $scope.index;
    $scope.size = 0;
    $scope.idregions;
    $scope.data.regionchoisit
    $scope.data.cache = true;
    $scope.data.profile = 'limite';
    $scope.getOptPays = function (option) {
      return option;
    };


    $scope.cacheselect = function () {
      if ($scope.data.cache) {
        $scope.data.cache = false;
      } else {
        $scope.data.cache = true;
      }

    }
    $scope.getOptRegion = function (option) {
      //   console.log($scope.data.regionchoisit)
      return option;
    };
    $scope.testProfile = function () {
      if (localStorage.getItem('loggedin_profil') == 'Codir YUP Mgt' || localStorage.getItem('loggedin_profil') == 'Direction Commerciale YUP Mgt'
        || localStorage.getItem('loggedin_profil') == 'Marketing YUP Mgt' || localStorage.getItem('loggedin_profil') == 'Call Center YUP Mgt'
        || localStorage.getItem('loggedin_profil') == 'Administrateur Maintenance') {
        // $scope.data.profile = 'super';
        $scope.data.profile = 'super';
      }
      // return profil;
      // $scope.data.profile = ProfilUser.profilUser();
    }
    $scope.checkConnect = function () {
      $scope.connect = ChekConnect.getConnectivite();
      $scope.testProfile();
    }
    $scope.checkConnect();
    $scope.initMap = function () {
      var options = {
        timeout: 10000,
        enableHighAccuracy: true
      };
      $cordovaGeolocation.getCurrentPosition(options).then(function (position) {

        var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

        var mapOptions = {
          center: latLng,
          zoom: 12,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        $scope.map = new google.maps.Map(document.getElementById("map2"), mapOptions);


        //Wait until the map is loaded
        google.maps.event.addListenerOnce($scope.map, 'idle', function () {

          var marker = new google.maps.Marker({
            map: $scope.map,
            animation: google.maps.Animation.DROP,
            position: latLng,
            icon: 'img/marker.png'
          });
        });
      });
    }
    $scope.listpays = function () {
      // $scope.data.profil = ProfilUser.profilUser();
      $scope.initMap();
      var pays;
      var listdespays;
      var payschoisit;
      if (window.Connection) {
        if (navigator.connection.type == Connection.NONE) {
          connect = false;

        }
        else {

          connect = true;

          if ($scope.data.profile == 'super') {
            var url = urlPhp.getUrl();
            $http.get(url + "/pays.php")
              .success(function (response) {
                // $ionicLoading.hide();
                console.log(response)
                pays = response;

                $scope.data.listpays = [];
                for (var i = 0; i < response.length; i++) {
                  var pv = { name: response[i].pays, id: response[i].idpays, code: response[i].code }
                  $scope.data.listpays.push(pv);
                }
                console.log($scope.data.listpays)
                localStorage.setItem('paysOnline', angular.toJson($scope.data.listpays));
              }).catch(function (error) {
                // $ionicLoading.hide();
                console.log(error)
              });
            //
          } else {
            //Recuperer la liste des pays
            var url = urlPhp.getUrl();
            $http.get(url + "/paysByUser.php?idutilisateurs=" + sessionStorage.getItem('loggedin_iduser'))
              .success(function (response) {
                //  $ionicLoading.hide();
                pays = response;
                //  localStorage.setItem('paysOnline', angular.toJson(pays));
                $scope.data.listpays = [];
                console.log(response)
                for (var i = 0; i < response.length; i++) {
                  var pv = { name: response[i].pays, id: response[i].idpays, code: response[i].code }
                  $scope.data.listpays.push(pv);
                }
                if ($scope.data.listpays.length != 0) {
                  //  payschoisit = $scope.data.listpays[0];
                  $scope.data.payschoisit = $scope.data.listpays[0];
                  //  $scope.listpointdevnte();
                  $scope.listDesregionsByPaysID();
                }

                console.log($scope.data.payschoisit)
                // $scope.listDesregionsByPaysID();
              }).catch(function (error) {
                // $ionicLoading.hide();
              });
            //Recuperer la liste des villes
          }

        }
      }

    }
    $scope.listDesregionsByPaysID = function () {

      if ($scope.connect == true) {
        //Recuperer la liste des regions
        var url = urlPhp.getUrl();
        $http.get(url + "/regionsByPays.php?idpays=" + $scope.data.payschoisit.id)
          .success(function (response) {
            $ionicLoading.hide();
            $scope.region = response;
            //  localStorage.setItem('regionsOnline', angular.toJson($scope.region));
            $scope.listregions = [];
            for (var i = 0; i < response.length; i++) {
              var pv = { name: response[i].region, id: response[i].idregion }
              $scope.listregions.push(pv);
            }

          }).catch(function (error) {
            $ionicLoading.hide();

          });
      }
    }
    $scope.refreshregion = function () {
      $scope.initMap();
      $scope.listregions = null
      $scope.data.regionchoisit = null

    }

    $scope.listpays();
    $scope.listpointdevnte = function () {
      $scope.pvtempon = [];
      $scope.data.pvchoisit = null;
      console.log($scope.data.payschoisit.code);
      var url = urlJava.getUrl();
      var link = url + "/yup/mespointVente"
      iduser = sessionStorage.getItem('loggedin_iduser')
      var user = {
        user: {
          "nom": "",
          "prenom": "",
          "telephone": "",
          "langue": "",
          "pays": "",
          "profil": "",
          "reseauxagent": "",
          "login": "",
          "password": "",
          "id": "" + iduser,
          "codePays": $scope.data.payschoisit.code
        }
      }
      //   console.log(user)
      $ionicLoading.show({ content: 'Loading', animation: 'fade-in', showBackdrop: true, maxWidth: 200, showDelay: 0, duration: 10000 });
      $http.post(link, user).then(function (res) {
        var options = {
          timeout: 10000,
          enableHighAccuracy: true
        };
        $ionicLoading.hide();
        $scope.pointvente = angular.fromJson(res.data).sort();
        if ($scope.pointvente && $scope.pointvente.length > 0) {
          console.log("depart ")
          console.log($scope.pointvente)

          $scope.pvv = [];
          $scope.pvvv = [];
          $cordovaGeolocation.getCurrentPosition(options).then(function (position) {

            var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

            var mapOptions = {
              center: latLng,
              zoom: 12,
              mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            $scope.map = new google.maps.Map(document.getElementById("map2"), mapOptions);


            //Wait until the map is loaded
            google.maps.event.addListenerOnce($scope.map, 'idle', function () {

              var marker = new google.maps.Marker({
                map: $scope.map,
                animation: google.maps.Animation.DROP,
                position: latLng,
                icon: 'img/marker.png'
              });
              $ionicLoading.show({ content: 'Loading', animation: 'fade-in', showBackdrop: true, maxWidth: 200, showDelay: 0, duration: 10000 });
              while ($scope.pvtempon.length < $scope.pointvente.length - 1) {

                if ($scope.size == 0) {
                  $scope.index = 0;
                  $scope.size = 10;
                } else {
                  $scope.index = $scope.size;
                  $scope.size = $scope.size + 10;
                }

                $scope.pvv = $scope.pointvente.slice($scope.index, $scope.size);
                $scope.pvtempon = $scope.pvtempon.concat($scope.pvv)
                $scope.pvv.forEach(function (pv) {
                  if (pv.latitude !== 0 && pv.longitude !== 0 && pv.latitude !== '' && pv.longitude !== '' && pv.latitude !== null && pv.longitude !== null) {
                    var marker = new google.maps.Marker({
                      map: $scope.map,
                      animation: google.maps.Animation.DROP,
                      position: new google.maps.LatLng(pv.latitude, pv.longitude),
                      icon: 'img/map-marker.png'
                    });

                    var infoWindow = new google.maps.InfoWindow({
                      content: "Point: " + pv.client + "<br/>Code: " + pv.codePointVente + "<br/>Telephone: " + pv.telephone + "<br/>Longitude: " + pv.longitude + "<br/>Latitude: " + pv.latitude
                    });

                    google.maps.event.addListener(marker, 'click', function () {
                      infoWindow.open($scope.map, marker);
                    })

                  }

                });
              }
              $ionicLoading.hide();


            });
          });
        }


      });

    }

    $scope.listPvPhp = function () {
      console.log("ID POINT ET ID REGION")
      console.log("user= " + sessionStorage.getItem('loggedin_iduser') + "   region= " + $scope.data.regionchoisit.id)
      var url = urlPhp.getUrl();
      $ionicLoading.show({ content: 'Loading', animation: 'fade-in', showBackdrop: true, maxWidth: 200, showDelay: 0, duration: 10000 });
      $http.get(url + '/pointventesutilisateurmap.php?idutilisateurs=' + sessionStorage.getItem('loggedin_iduser') + "&idregions=" + $scope.data.regionchoisit.id).then(function (res) {
        var options = {
          timeout: 10000,
          enableHighAccuracy: true
        };

        $scope.pointventes = angular.fromJson(res.data).sort();

        $cordovaGeolocation.getCurrentPosition(options).then(function (position) {

          var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

          var mapOptions = {
            center: latLng,
            zoom: 12,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          };

          $scope.map = new google.maps.Map(document.getElementById("map2"), mapOptions);


          //Wait until the map is loaded
          google.maps.event.addListenerOnce($scope.map, 'idle', function () {

            var marker = new google.maps.Marker({
              map: $scope.map,
              animation: google.maps.Animation.DROP,
              position: latLng,
              icon: 'img/marker.png'
            });
            console.log($scope.pointventes)
            $scope.pointventes.forEach(function (pv) {
              var marker = new google.maps.Marker({
                map: $scope.map,
                animation: google.maps.Animation.DROP,
                position: new google.maps.LatLng(pv.latitude, pv.longitude),
                icon: 'img/map-marker.png'
              });

              var infoWindow = new google.maps.InfoWindow({
                content: "Point: " + pv.client + "<br/>Code: " + pv.codePointVente + "<br/>Telephone: " + pv.telephone + "<br/>Longitude: " + pv.longitude + "<br/>Latitude: " + pv.latitude
              });

              google.maps.event.addListener(marker, 'click', function () {
                infoWindow.open($scope.map, marker);
              })
              $ionicLoading.hide();
            });
          });
        });
      });
    }
  })
  .controller('MesprospectsCtrl', function ($scope, $http, $ionicLoading, ChekConnect, $ionicPopup, $translate, urlPhp) {
    $scope.connect = null;
    $scope.data = {};
    //Tester la connectiviteee
    $scope.checkConnect = function () {
      $scope.connect = ChekConnect.getConnectivite();
    }
    //Tester la connectiviteee
    $scope.checkConnect();
    if ($scope.connect == true) {
      $scope.data.connect = true;
      $ionicLoading.show({ content: 'Loading', animation: 'fade-in', showBackdrop: true, maxWidth: 200, showDelay: 0, duration: 3000 });
      var url = urlPhp.getUrl();
      $http.get(url + "/mesprospects.php?idutilisateurs=" + sessionStorage.getItem('loggedin_iduser'))
        .success(function (response) {
          $ionicLoading.hide();
          var inverse = []
          var taille = response.length - 1
          for (var i = taille; i >= 0; i--) {
            inverse.push(response[i])
            console.log(response[i].Date)
          }
          console.log(inverse)
          $scope.synchronDataLocalProspect()
          $scope.mesprospects = inverse;
          console.log($scope.mesprospects);
        }).catch(function (error) {
          $ionicLoading.hide();
          alert(error);
        });
    } else {
      $scope.data.connect = false;
      $translate('alert_header_ofline').then(function (header) {
        $translate('alert_content_ofline_list').then(function (content) {
          $translate('alert_button_non').then(function (non) {
            $translate('alert_button_oui').then(function (oui) {
              $ionicPopup.show({
                title: header,
                content: content,
                buttons: [
                  {
                    text: non,
                    type: 'button-assertive',
                    onTap: function (e) {
                      return false;
                    }
                  },
                  {
                    text: oui,
                    type: 'button-energized',
                    onTap: function (e) {
                      return true;
                    }
                  }]
              })
                .then(function (result) {
                  if (!result) {
                    ionic.Platform.exitApp();
                  } else {
                    $ionicLoading.show({ content: 'Loading', animation: 'fade-in', showBackdrop: true, maxWidth: 200, showDelay: 0, duration: 10000 });
                    var flocal = angular.fromJson(localStorage.getItem('prospectsSauvegarde'));
                    console.log(flocal)
                    $scope.mesprospectsocal = flocal;
                    $ionicLoading.hide();
                  }
                });
            });
          });
        });
      });

    }

    //Gerer les donnees locales: Synchronisation

    $scope.synchronDataLocalProspect = function () {
      //  $ionicLoading.show({ content: 'Tentative de synchronisation', animation: 'fade-in', showBackdrop: true, maxWidth: 800, showDelay: 0, timeout: 10000 });
      var flocal = angular.fromJson(localStorage.getItem('prospectsSauvegarde'));
      if (flocal.length == 0) {
        console.log('Pas de donnees locales')
        //  $ionicLoading.hide();
      } else {
        //  var echec = [];

        $translate('header_title_synchrone').then(function (header) {
          $translate('content_label_synchrone').then(function (content) {
            $translate('content_label_synchrone_nb_fiche').then(function (nb) {
              $translate('content_label_synchrone_nb_suggetion').then(function (sugest) {
                var echec = [];
                $ionicPopup.show({
                  title: header,
                  template: content + "<br/>" + nb + flocal.length + "<br/>" + sugest,
                  scope: $scope,
                  buttons: [
                    {
                      text: 'OK',
                      type: 'button-energized',
                      onTap: function (e) {
                        return true;
                      }
                    }]
                }).then(function (result) {
                  if (!result) {

                  } else {
                    // console.log(flocal[0])
                    var url = urlPhp.getUrl();
                    $ionicLoading.show({ content: 'Tentative de synchronisation', animation: 'fade-in', showBackdrop: true, maxWidth: 800, showDelay: 0, timeout: 3000 });
                    for (var i = 0; i < flocal.length; i++) {
                      $http.post(url + '/prospect.php', flocal[i]).then(function (res) {
                        console.log('index :' + i + " " + res.data)
                      }).catch(function (error) {
                        echec.push(flocal[i])
                        console.log('echoue')
                        $ionicLoading.hide();
                        alert('Synchro echouée');
                      });
                    }
                    console.log('Taille apres la boucle')
                    console.log(echec)
                    $ionicLoading.hide();

                    if (echec.length == 0) {
                      var init = [];
                      console.log('tout est envoyé')
                      $translate('alert_header_reussi').then(function (header) {
                        $translate('alert_content_reussi').then(function (content) {
                          $ionicPopup.confirm({
                            title: header,
                            content: content,
                            buttons: [
                              {
                                text: 'OK',
                                type: 'button-energized',
                                onTap: function (e) {
                                  return true;
                                }
                              }]
                          })
                            .then(function (result) {
                              if (result) {
                                localStorage.setItem('prospectsSauvegarde', angular.toJson(init))
                              } else {
                                localStorage.setItem('prospectsSauvegarde', angular.toJson(init))
                              }
                            });
                        })
                      })


                    } else {
                      console.log('Taille s il ya erreur')
                      console.log(echec)
                      localStorage.setItem('prospectsSauvegarde', angular.toJson(echec))
                    }
                  }
                })
              })
            })
          })
        })

      }
    }

  })
  .controller('ListpointventeCtrl', function ($scope, $http, $ionicLoading,
    ChekConnect, $ionicPopup, $translate,
    ApiListPays, ModelPays, ApiListRegion, ModelRegions,
    ApiListPointVente, ModelPointVente, $state,
    ApiListZonesSuper,
    ApiListPointventeParZone

  ) {
    $scope.motcle;
    $scope.shouldShowDelete = false;
    $scope.shouldShowReorder = false;
    $scope.listCanSwipe = true;
    $scope.data.payschoisit = null;
    $scope.pvtempon = [];
    $scope.index;
    $scope.size = 0;
    $scope.idregions;
    $scope.data.regionchoisit
    $scope.data.zonechoisit = null
    $scope.data.cache = true;
    $scope.data.profile = 'limite';
    $scope.data.hide = true;
    $scope.data.listpointvente = [];
    $scope.listregions = [];
    $scope.data.listZones = [];
    $scope.getOptPays = function (option) {
      return option;
    };
    $scope.cacheselect = function () {
      if ($scope.data.cache) {
        $scope.data.cache = false;
      } else {
        $scope.data.cache = true;
      }

    }
    $scope.getOptRegion = function (option) {
      //   console.log($scope.data.regionchoisit)
      return option;
    };
    $scope.testProfile = function () {
      if (localStorage.getItem('loggedin_profil') == 'Codir YUP Mgt' || localStorage.getItem('loggedin_profil') == 'Direction Commerciale YUP Mgt'
        || localStorage.getItem('loggedin_profil') == 'Marketing YUP Mgt' || localStorage.getItem('loggedin_profil') == 'Call Center YUP Mgt'
        || localStorage.getItem('loggedin_profil') == 'Administrateur Maintenance') {

        $scope.data.profile = 'super';
      }
      // return profil;
      // $scope.data.profile = ProfilUser.profilUser();
    }
    $scope.checkConnect = function () {
      $scope.connect = ChekConnect.getConnectivite();
      $scope.testProfile();

    }
    $scope.checkConnect();
    //Chargement des pays
    $scope.listzonesSuper = function () {
      //$scope.listZones =
      var list = [];
      $ionicLoading.show({ content: 'Loading', animation: 'fade-in', showBackdrop: true, maxWidth: 200, showDelay: 0, duration: 10000 });
      ApiListZonesSuper
        .getListZone($scope.data.payschoisit.id).success(response => {
          console.log(response)
          $ionicLoading.hide();
          if (response) {
            for (var i = 0; i < response.length; i++) {
              list.push({ name: response[i].libelle, id: response[i].idzone });
            }

            $scope.data.listZones = list;
            console.log($scope.data.listZones)
          }

        });


    }
    $scope.listPays = function () {
      $scope.data.listpays = [];
      ApiListPays
        .getListpays($scope.data.profile)
        .success(function (response) {
          console.log("Appel ctrl")
          if (response && response.length > 0) {
            //Structurer les objet pays
            $scope.data.listpays = ModelPays.getModelPays(response);
            if ($scope.data.listpays.length == 1) {
              $scope.data.payschoisit = $scope.data.listpays[0];
              $scope.listzonesSuper();
            }
          }
        })
    }
    $scope.listPays();
    //Chargement des regions
    $scope.listDesregionsByPaysID = function () {
      console.log('charger liste des regions');

      var connexion;
      if (window.cordova) {
        connexion = $scope.connect
      } else {
        connexion = true;
      }
      if (connexion == true) {
        //Recuperer la liste des regions
        console.log('charger liste des regions1')

        ApiListRegion
          .getListRegionsByPays($scope.data.payschoisit.id)
          .success(function (response) {
            $scope.listregions = null;

            $scope.listregions = ModelRegions.getModelRegions(response);
          })
          .catch(function (error) {
            console.log(error)
          })

      }
    }
    $scope.refreshregion = function () {
      $scope.listregions.length = 0;
      if ($scope.data.listpointvente) {
        $scope.data.listpointvente.length = 0;
      }
      $scope.data.regionchoisit = null
      $scope.data.zonechoisit = null;
      $scope.listzonesSuper();

    }
    //Chargement des point par zone

    $scope.listPvParZone = function () {
      console.log('zone')
      if ($scope.data.listpointvente) {
        $scope.data.listpointvente.length = 0;
      }
      $ionicLoading.show({ content: 'Loading', animation: 'fade-in', showBackdrop: true, maxWidth: 200, showDelay: 0, duration: 10000 });
      ApiListPointventeParZone
        .pointventParZone($scope.data.payschoisit.id, $scope.data.zonechoisit.id)
        .success(function (response) {
          if (response) {
            $ionicLoading.hide();
            $scope.data.listpointvente = response
            console.log(response)
            //$scope.listpointvente = $scope.listpointvente[0]
          }
        })
    }
    //Chargement des point de vente
    $scope.listPvPhp = function () {
      $scope.listpointvente.length = 0;
      console.log("ID POINT ET ID REGION")
      console.log("user= " + sessionStorage.getItem('loggedin_iduser') + "   region= " + $scope.data.regionchoisit.id)
      //$ionicLoading.show({ content: 'Loading', animation: 'fade-in', showBackdrop: true, maxWidth: 200, showDelay: 0, duration: 10000 });
      ApiListPointVente
        .getListPointVente(sessionStorage.getItem('loggedin_iduser'), $scope.data.regionchoisit.id)
        .success(function (response) {
          if (response) {
            $scope.listpointvente = ModelPointVente.getModelPointVente(response);
            console.log($scope.listpointvente)
            //$scope.listpointvente = $scope.listpointvente[0]
          }
        })
    }
    $scope.hidePaysRegions = function () {
      if ($scope.data.hide) {
        $scope.data.hide = false
      } else {
        $scope.data.hide = true
      }
    }

    $scope.goToPlusInfoPv = function (item) {

      var poinvente =item;
      console.log(poinvente)
      if(poinvente == null){
      /* var pvt= {
          idpointVentes: null,
          adresse: '',
          altitude: null,
          client: '',
          codePointVente: '',
          dateAjout: null,
          email: '',
          latitude: 0,
          longitude: 0,
          pointVente: '',
          telephone: '',
          codePays: null,
          responsable: null,
          stagiaire1: null,
          stagiaire2: null,
          stagiaire3: null,
          statutagent: null,
          archive: null,
          ville: null,
          typePointVente: null,
          zone: null,
        }*/
        poinvente = null;
      }
      $state.go('app.pointvente', {
        pvt: poinvente
      });

    }
  })
  .controller('PointventeCtrl', function ($scope, $ionicPopup, $timeout, $cordovaGeolocation, $ionicLoading, ChekConnect, $translate, ApiListTypePointVente,urlPhp,
    $http,
    ApiListZonesSuper,
    ApiListZonesLimite,
    ApiModifPoint,
    $filter,
    ApiListPointVenteJava,
    $state,
    $stateParams,
    urlJava
  ) {
    $scope.data = {};
    $scope.pointvent;
    $scope.pointventTempon;
    $scope.hideCancel = false;
    $scope.data.profil = 'limite';
    $scope.data.payschoisit;
    $scope.listvilles = [];
    $scope.listregions = [];
    $scope.data.itemszones = [];
    $scope.data.regionpointvent = null;


    console.log('Le poit etat')
    console.log($stateParams.pvt)
    $scope.initData = function () {
      if (angular.isUndefined($stateParams.pvt) || $stateParams.pvt==null) {
        {
          $scope.pointvent = {
            idpointVentes: null,
            adresse: '',
            altitude: null,
            client: '',
            codePointVente: '',
            dateAjout: null,
            email: '',
            latitude: 0,
            longitude: 0,
            pointVente: '',
            telephone: '',
            codePays: null,
            responsable: null,
            stagiaire1: null,
            stagiaire2: null,
            stagiaire3: null,
            statutagent: null,
            archive: null,
            ville: null,
            typePointVente: null,
            zone: null,
          }
          $scope.pointventTempon = $scope.pointvent;
          $scope.data.typeValidation = 'Ajout';
        }
      } else {
        $scope.pointvent = $stateParams.pvt;
        $scope.pointventTempon = $stateParams.pvt;
        $scope.data.regionpointvent = $scope.pointvent.ville.region;
        $scope.data.typeValidation = 'Modification';

      }
      $scope.checkProfil();
      $scope.initMap();
      $scope.listpays();
      $scope.ListTypePointVente();
    }
    $scope.initMap = function () {
      console.log()

      var options = {
        timeout: 10000,
        enableHighAccuracy: true
      };
      if (angular.isUndefined(new google.maps.LatLng($scope.pointvent.latitude, $scope.pointvent.longitude))) {

      } else {
        var latLng = new google.maps.LatLng($scope.pointvent.latitude, $scope.pointvent.longitude);

        var mapOptions = {
          center: latLng,
          zoom: 12,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        $scope.map = new google.maps.Map(document.getElementById("mapf"), mapOptions);


        //Wait until the map is loaded
        google.maps.event.addListenerOnce($scope.map, 'idle', function () {

          var marker = new google.maps.Marker({
            map: $scope.map,
            animation: google.maps.Animation.DROP,
            position: latLng,
            icon: 'img/marker.png'
          });
        });

      }




    }

    $scope.checkProfil = function () {

      if (localStorage.getItem('loggedin_profil') == 'Codir YUP Mgt' || localStorage.getItem('loggedin_profil') == 'Direction Commerciale YUP Mgt'
        || localStorage.getItem('loggedin_profil') == 'Marketing YUP Mgt' || localStorage.getItem('loggedin_profil') == 'Call Center YUP Mgt'
        || localStorage.getItem('loggedin_profil') == 'Administrateur Maintenance') {
        // $scope.data.profile = 'super';
        console.log(localStorage.getItem('loggedin_profil'));
        $scope.data.profil = 'super';
      } else {
        console.log('limite');
        $scope.data.payschoisit = localStorage.getItem('loggedin_pays');
      }
    }
    $scope.listpays = function () {

      var pays;
      var listdespays;
      var payschoisit;

      connect = true;

      if ($scope.data.profil == 'super') {
        console.log($scope.data.profil)
        var url = urlPhp.getUrl();
        $http.get(url + "/pays.php")
          .success(function (response) {
            // $ionicLoading.hide();
            console.log(response)
            pays = response;

            $scope.data.listpays = [];
            for (var i = 0; i < response.length; i++) {
              var pv = { name: response[i].pays, id: response[i].idpays, code: response[i].code }
              $scope.data.listpays.push(pv);
            }
            console.log($scope.data.listpays)
            localStorage.setItem('paysOnline', angular.toJson($scope.data.listpays));
          }).catch(function (error) {
            // $ionicLoading.hide();
            console.log(error)
          });
        //
      } else {
        //Recuperer la liste des pays
        console.log('limite')
        var url = urlPhp.getUrl();
        $http.get(url + "/paysByUser.php?idutilisateurs=" + sessionStorage.getItem('loggedin_iduser'))
          .success(function (response) {
            //  $ionicLoading.hide();
            console.log(response)
            pays = response;
            //  localStorage.setItem('paysOnline', angular.toJson(pays));
            $scope.data.listpays = [];
            console.log(response)
            for (var i = 0; i < response.length; i++) {
              var pv = { name: response[i].pays, id: response[i].idpays, code: response[i].code }
              $scope.data.listpays.push(pv);
            }
            if ($scope.data.listpays.length != 0) {
              //  payschoisit = $scope.data.listpays[0];
              $scope.data.payschoisit = $scope.data.listpays[0];
              //  $scope.listpointdevnte();
              $scope.listDesregionsByPaysID($scope.data.payschoisit.id);
              $scope.listzonesSuper($scope.data.payschoisit.id);
            }

            console.log($scope.data.payschoisit)

          }).catch(function (error) {
            // $ionicLoading.hide();
          });
        //Recuperer la liste des villes
      }


    }
    $scope.listDesregionsByPaysID = function (idpays) {
      var url = urlPhp.getUrl();
      $http.get(url + "/regionsByPays.php?idpays=" + idpays)
        .success(function (response) {
          $ionicLoading.hide();
          if (response) {
            $scope.region = response;
            //localStorage.setItem('regionsOnline', angular.toJson($scope.region));
            $scope.listregions = [];
            $scope.listregions = response;
            /* for (var i = 0; i < response.length; i++) {
               var pv = { name: response[i].region, id: response[i].idregion }
               $scope.listregions.push(pv);
             }*/
            console.log($scope.listregions);
            $scope.data.itemsregions = $scope.listregions;
          }

        }).catch(function (error) {
          $ionicLoading.hide();

        });

    }
    $scope.refreshregion = function () {

      $scope.listregions = null
      $scope.data.regionchoisit = null

    }


    $scope.listVillesByRegionID = function (idregions) {
      console.log('id regions')

      var url = urlJava.getUrl();
      $http.get(url + "/yup/villes/" + idregions)
        .success(function (response) {
          $ionicLoading.hide();
          console.log(response)
          // localStorage.setItem('villesOnline', angular.toJson($scope.ville));
          $scope.listvilles = [];
          if (response) {
            $scope.listvilles = response;
          }

          /*for (var i = 0; i < response.length; i++) {
            var pv = { name: response[i].ville, id: response[i].idville }
            $scope.listvilles.push(pv);
          }*/
          //    console.log($scope.listvilles)
        }).catch(function (error) {
          $ionicLoading.hide();
        });

    }
    $scope.listzonesSuper = function (idpays) {
      //$scope.listZones =
      ApiListZonesSuper
        .getListZone(idpays).success(response => {
          if (response) {
            $scope.listZones = response;
          }
          console.log(response)
        });


    }

    $scope.showPopup = function (header, codeInput, value) {
      $scope.data.text = value;
      $scope.data.numerique;
      switch (codeInput) {
        case 'client':
          $scope.data.text = $scope.pointvent.client;
          $scope.data.numerique = false;
          break;
        case 'email':
          $scope.data.text = $scope.pointvent.email;
          $scope.data.numerique = false;
          break;
        case 'pointvent':
          $scope.data.text = $scope.pointvent.pointVente;
          $scope.data.numerique = false;
          break;
        case 'codeAgent':
          $scope.data.text = $scope.pointvent.codePointVente;
          $scope.data.numerique = false;
          break;
        case 'adresse':
          $scope.data.text = $scope.pointvent.adresse;
          $scope.data.numerique = false;
          break;
        case 'telephone':
          $scope.data.text = $scope.pointvent.telephone;
          $scope.data.numerique = true;
          break;

      }
      var myPopup = $ionicPopup.show({
        template: '<input ng-if="!data.numerique" type="text" ng-model="data.text"><input ng-if="data.numerique" type="number" ng-model="data.text">',
        title: header,
        subTitle: 'Taper et enregistrer',
        scope: $scope,
        buttons: [
          {
            text: 'Annuler',
            onTap: function (e) {
              return $scope.data.text;
            }
          },
          {
            text: '<b>Enregistrer</b>',
            type: 'button-positive',
            onTap: function (e) {
              return $scope.data.text;
            }
          },
        ]
      });
      myPopup.then(function (res) {
        console.log(codeInput)
        switch (codeInput) {
          case 'client':
            $scope.pointvent.client = res;
            console.log('Tapped!', res);
            break;
          case 'email':
            $scope.pointvent.email = res;
            console.log('Tapped!', res);
            break;
          case 'pointvent':
            $scope.pointvent.pointVente = res;
            console.log('Tapped!', res);
            break;
          case 'codeAgent':
            $scope.pointvent.codePointVente = res;
            console.log('Tapped!', res);
            break;
          case 'adresse':
            $scope.pointvent.adresse = res;
            console.log('Tapped!', res);
            break;
          case 'telephone':
            $scope.pointvent.telephone = res;
            console.log('Tapped!', res);
            break;

        }
        console.log($scope.pointvent)
      });

    };
    $scope.showPopupSelectZone = function (header, codeInput, value) {
      console.log($scope.data.listpays)
      $scope.data.itemspays = $scope.data.listpays;

      if ($scope.pointvent.zone) {
        $scope.data.textzone = $scope.pointvent.zone.libelle;
      }

      $scope.data.textpays;
      $scope.data.type = 'zone'
      var myPopup = $ionicPopup.show({
        template: '<input type="text" ng-model="data.textzone">' + '<br/>' +
          '<div ng-click="showPopupSelectPays(data.type)"  ng-if="data.itemspays.length > 1">' +
          '<i >Choisir Pays</i><br/>' +
          '</div><br/>' +
          'Choisir Zone<br/>' +
          '<select ng-model="data.textzone">' +
          '<option ng-repeat="item in listZones">{{item.libelle}}</option>' +
          '</select>',
        title: header,
        subTitle: 'Taper et enregistrer',
        scope: $scope,
        buttons: [
          {
            text: 'Annuler',
            onTap: function (e) {
              return $scope.data.textzone;
            }
          },
          {
            text: '<b>Enregistrer</b>',
            type: 'button-positive',
            onTap: function (e) {
              return $scope.data.textzone;

            }
          },
        ]
      });

      myPopup.then(function (res) {
        $scope.listZones.forEach(zone => {
          if (zone.libelle == res) {
            $scope.zonechoisit = zone;
          }
        });
        $scope.pointvent.zone = $scope.zonechoisit;
        console.log('l objet point vente')
        console.log($scope.pointvent)
      });
    };
    $scope.showPopupSelect = function (header, codeInput, value) {

      $scope.data.items = $scope.listtypepointvent;
      if ($scope.pointvent.typePointVente !== null && angular.isUndefined($scope.pointvent.typePointVente == false)) {
        $scope.data.texttypoint = $scope.pointvent.typePointVente.libelle;
      }

      var myPopup = $ionicPopup.show({
        template: '<input type="text" ng-model="data.texttypoint" ng-readonly="true"> ' + '<br/>' +
          '<select ng-model="data.texttypoint">' +
          '<option ng-repeat="item in data.items">{{item.libelle}}</option>' +
          '</select>',
        title: header,
        subTitle: 'Taper et enregistrer',
        scope: $scope,
        buttons: [
          {
            text: 'Annuler',
            onTap: function (e) {

              return $scope.data.texttypoint;

            }
          },
          {
            text: '<b>Enregistrer</b>',
            type: 'button-positive',
            onTap: function (e) {

              return $scope.data.texttypoint;

            }
          },
        ]
      });
      myPopup.then(function (res) {
        console.log(codeInput)
        switch (codeInput) {
          case 'typepointvente':
            $scope.listtypepointvent.forEach(type => {
              if (type.libelle == res) {
                $scope.typepointchoisit = type;
              }
            });
            $scope.pointvent.typePointVente = $scope.typepointchoisit;
            break;
        }

      });

    };
    $scope.showPopupSelectRegion = function (header, codeInput, value) {
      $scope.data.itemsregions = $scope.listregions;
      $scope.data.itemspays = $scope.data.listpays;
      if ($scope.pointvent.ville && $scope.pointvent.ville.region) {
        $scope.data.textregions = $scope.data.regionpointvent.region
      }
      $scope.data.textpays;
      $scope.data.type = 'region'
      console.log($scope.listregions)
      var myPopup = $ionicPopup.show({
        template: '<input type="text" ng-model="data.textregions" ng-readonly="true">' + '<br/>' +
          '<div ng-click="showPopupSelectPays(data.type)"  ng-if="data.itemspays.length > 1">' +
          '<i >Choisir Pays</i><br/>' +
          '</div><br/>' +
          'Choisir Region<br/>' +
          '<select ng-model="data.textregions">' +
          '<option ng-repeat="item in data.itemsregions">{{item.region}}</option>' +
          '</select>',
        title: header,
        subTitle: 'Taper et enregistrer',
        scope: $scope,
        buttons: [
          {
            text: 'Annuler',
            onTap: function (e) {

              return $scope.data.textregions;

            }
          },
          {
            text: '<b>Enregistrer</b>',
            type: 'button-positive',
            onTap: function (e) {
              return $scope.data.textregions;
            }
          },
        ]
      });

      myPopup.then(function (res) {
        console.log($scope.listregions)
        if ($scope.listregions.length > 0) {

          $scope.listregions.forEach(region => {
            if (region.region == res) {
              $scope.regionchoisit = region;
            }
          });
          if ($scope.regionchoisit !== null && angular.isUndefined($scope.regionchoisit) == false) {
            $scope.listvilles.length = 0;
            console.log($scope.regionchoisit)
            $scope.data.regionpointvent = $scope.regionchoisit;
            $scope.listVillesByRegionID($scope.regionchoisit.idregion);
          }

        } else {
          $scope.data.regionpointvent = $scope.pointvente.ville.region;
        }

      });
    };
    $scope.showPopupSelectVille = function (header, codeInput, value) {
      $scope.data.items = $scope.listvilles;
      if ($scope.pointvent.ville) {
        $scope.data.textville = $scope.pointvent.ville.ville;
      }

      var myPopup = $ionicPopup.show({
        template: '<label style="color:red" ng-if="!data.regionpointvent">Selectionner une region avant de.</label>' +
          '<input ng-if="data.regionpointvent" type="text" ng-model="data.textville" ng-readonly="true">' + '<br/>' +
          'Choisir ville<br/><select ng-model="data.textville">' +
          '<option ng-repeat="item in data.items">{{item.ville}}</option>' +
          '</select>',
        title: header,
        subTitle: 'Taper et enregistrer',
        scope: $scope,
        buttons: [
          {
            text: 'Annuler',
            onTap: function (e) {

              return $scope.data.textville;

            }
          },
          {
            text: '<b>Enregistrer</b>',
            type: 'button-positive',
            onTap: function (e) {
              return $scope.data.textville;
            }
          },
        ]
      });
      myPopup.then(function (res) {
        console.log(res)

        if ($scope.listvilles.length > 0) {
          $scope.listvilles.forEach(ville => {
            if (ville.ville == res) {
              $scope.villechoisit = ville;
            }
          });
          $scope.pointvent.ville = $scope.villechoisit;
        } else {
          if ($scope.pointvent.ville) {
            $scope.data.textville = $scope.pointvent.ville.ville;
          }

        }
        console.log($scope.pointvent.ville)

      });
    };
    $scope.showPopupSelectPays = function (typepasse) {

      $scope.data.items = $scope.data.listpays
      $scope.data.text = '';
      var myPopup = $ionicPopup.show({
        template: 'Choisir pays<br/><select ng-model="data.text">' +
          '<option ng-repeat="item in data.items">{{item.name}}</option>' +
          '</select>',
        title: '',
        subTitle: 'Taper et enregistrer',
        scope: $scope,
        buttons: [
          {
            text: 'Annuler',
            onTap: function (e) {
              if (!$scope.data.text) {
                e.preventDefault();
              } else {
                return $scope.data.text;
              }
            }
          },
          {
            text: '<b>Enregistrer</b>',
            type: 'button-positive',
            onTap: function (e) {
              if (!$scope.data.text) {
                e.preventDefault();
              } else {

                return $scope.data.text;
              }
            }
          },
        ]
      });
      myPopup.then(function (res) {
        $scope.data.listpays.forEach(pays => {
          if (pays.name == res) {
            $scope.payschoisit = pays;
          }
        });
        console.log('type passe')
        console.log(typepasse)
        if (typepasse == 'region') {
          $scope.data.itemsregions.length = 0;
          $scope.listDesregionsByPaysID($scope.payschoisit.id);
          console.log(res);
        } else {
          $scope.data.itemszones.length = 0;
          $scope.listzonesSuper($scope.payschoisit.id);
          console.log(res);
        }

      });
    };
    $scope.ListTypePointVente = function () {
      ApiListPointVenteJava.getListTypePointVente()
        .success(function (response) {
          if (response) {
            console.log(response)
            // console.log(angular.toJson(response))
            $scope.listtypepointvent = response;
            //$scope.listtypepointvent = ModelTypePointVente.getModelTypePointVente(response);
            console.log($scope.listtypepointvent);

          }
        })
    }


    $scope.popUPMethode = function (nature) {
      var header_nature = "";
      var content_nature = "";
      switch (nature) {
        case 'maj gps':
          header_nature = "pv_header_maj_coordonnes";
          content_nature = "pv_maj_coordonnees";
          break;
        case 'cancel gps':
          header_nature = "pv_header_maj_coordonnes";
          content_nature = "pv_cancel_maj_coordonnees";
          break;
      }
      $translate(header_nature).then(function (header) {
        $translate('alert_button_oui').then(function (oui) {
          $translate('alert_button_non').then(function (non) {
            $translate(content_nature).then(function (content) {
              $ionicPopup.show({
                title: header,
                content: content,
                buttons: [
                  {
                    text: non,
                    type: 'button-assertive',
                    onTap: function (e) {
                      return false;
                    }
                  },
                  {
                    text: oui,
                    type: 'button-energized',
                    onTap: function (e) {
                      return true;
                    }
                  }]
              })
                .then(function (result) {
                  if (!result) {

                  } else {
                    if (nature === 'maj gps') {
                      $scope.getLocation();
                    } else if (nature === 'cancel gps') {
                      console.log($scope.pointventTempon)
                      $scope.pointvent.latitude = $scope.pointventTempon.latitude;
                      $scope.pointvent.longitude = $scope.pointventTempon.longitude
                      $scope.hideCancel = false;
                      $scope.initMap();
                    }
                    // $scope.data.pvchoisit = $scope.lesplusproches[$scope.lesplusproches.length - 1];
                  }
                })
            })

          });
        });

      });
    }
    $scope.initForm = function () {
      $scope.pointvent = {
        idpointVentes: null,
        adresse: '',
        altitude: null,
        client: '',
        codePointVente: '',
        dateAjout: null,
        email: '',
        latitude: 0,
        longitude: 0,
        pointVente: '',
        telephone: '',
        codePays: null,
        responsable: null,
        stagiaire1: null,
        stagiaire2: null,
        stagiaire3: null,
        statutagent: null,
        archive: null,
        ville: null,
        typePointVente: null,
        zone: null,
      }
      $scope.pointventTempon = $scope.pointvent;
      $scope.data.typeValidation = 'Ajout';
      $scope.data.regionpointvent = null;
    }
    $scope.getLocation = function () {
      var options = {
        timeout: 10000,
        enableHighAccuracy: true
      };
      $cordovaGeolocation.getCurrentPosition(options).then(function (position) {
        $scope.pointvent.latitude = position.coords.latitude
        $scope.pointvent.longitude = position.coords.longitude
        $scope.hideCancel = true;
        $scope.initMap();
        //position.coords.latitude, position.coords.longitude
        //console.longitude(position.coords.latitude + " "+ position.coords.longitude)
      })
    }
    $scope.initData()
    var date = new Date();
    $scope.controlForm = function () {
      var result = 'complet';
      if ($scope.pointvent.client == '') {
        result = 'client';
      } else
        if ($scope.pointvent.adresse == '') {
          result = 'adresse';
        } else if ($scope.pointvent.email == '') {
          result = 'email';
        } else if ($scope.pointvent.pointVente == '') {
          result = 'Nom point vente';
        } else if ($scope.pointvent.codePointVente == '') {
          result = 'code Point Vente';
        } else if ($scope.pointvent.telephone == '') {
          result = 'telephone';
        } else if (angular.isUndefined($scope.pointvent.typePointVente) || $scope.pointvent.typePointVente == null) {
          result = 'type Point Vente';
        } else if (angular.isUndefined($scope.pointvent.ville) || $scope.pointvent.ville == null) {
          result = 'ville';
        } else if (angular.isUndefined($scope.pointvent.zone) || $scope.pointvent.zone == null) {
          result = 'zone';
        }
      return result;
    }
    $scope.savePointVente = function () {
      var headeralert = '';
      var contentalet = '';
      var resultat = $scope.controlForm();
      console.log(resultat)
      if (resultat == 'complet') {
        $scope.validForm();
      } else {
        console.log(resultat)
        console.log($scope.data.typeValidation)
        if ($scope.data.typeValidation == 'Ajout') {

          $ionicPopup.confirm({
            title: 'Formulaire vide',
            content: 'le champs ' + resultat + ' est non renseigné. Remplir tout et valider',
            buttons: [
              {
                text: 'OK',
                type: 'button-energized',
                onTap: function (e) {
                  return true;
                }
              }]
          })
            .then(function (result) {
              if (!result) {

              } else {

              }

            });
        } else {

          $ionicPopup.confirm({
            title: 'Formulaire vide',
            content: 'le champs ' + resultat + ' est non renseigné. Voullez-vous valider quand meme?',
            buttons: [
              {
                text: 'Annuler',
                type: 'button-assertive',
                onTap: function (e) {
                  return false;
                }
              },
              {
                text: 'OUI',
                type: 'button-energized',
                onTap: function (e) {
                  return true;
                }
              }]
          })
            .then(function (result) {
              if (result) {
                if (resultat == 'type Point Vente') {

                  $ionicPopup.confirm({
                    title: 'Formulaire vide',
                    content: 'le champs ' + resultat + ' est non renseigné. impossible de valider',
                    buttons: [
                      {
                        text: 'OK',
                        type: 'button-energized',
                        onTap: function (e) {
                          return true;
                        }
                      }]
                  })
                    .then(function (result) {
                      if (!result) {

                      } else {

                      }


                    });

                } else {
                  console.log('Ca entre ici')
                  console.log(resultat)
                  $scope.validForm();
                }
              } else {


              }
            });

        }

      }

    }
    $scope.validForm = function () {

      $scope.formatedDate = $filter("date")(new Date(), "yyyy-MM-dd HH:m:ss");

      var value = {
        "idDemande": 0,
        "adresse": $scope.pointvent.adresse,
        "codePointVente": $scope.pointvent.codePointVente,
        "dateAjout": $scope.formatedDate,
        "latitude": $scope.pointvent.latitude,
        "longitude": $scope.pointvent.longitude,
        "pointVente": $scope.pointvent.pointVente,
        "telephone": $scope.pointvent.telephone,
        "archive": false,
        "ville": {
          "idVille": $scope.pointvent.ville.idVille
        },
        "typePointVente": {
          "idtypepv": $scope.pointvent.typePointVente.idtypepv
        },
        "zone": {
          "idzone": $scope.pointvent.zone.idzone
        },
        "agent": {
          "idutilisateurs": sessionStorage.getItem('loggedin_iduser')
        },
        "etape": 0,
        "type": $scope.data.typeValidation
      }

      console.log('Point a valider')
      console.log($scope.pointvent)
      console.log(value)
      $ionicLoading.show({ content: 'Loading', animation: 'fade-in', showBackdrop: true, maxWidth: 200, showDelay: 0, duration: 10000 });
      ApiModifPoint.updatePointvente(value)
        .then(response => {
          $ionicLoading.hide();
          if (response.data === 'Reussi') {
            if ($scope.data.typeValidation == 'Ajout') {
              $scope.initForm();
            }

            $scope.alertValidtion('Reussi');
          }
          var result = "Echec" + response.data;
          console.log(result)
          if (result === "Echec") {
            $scope.alertValidtion('Echec');
          }

        })
    }
    $scope.alertValidtion = function (result) {
      var headeralert = '';
      var contentalet = '';
      if (result === 'Reussi') {
        headeralert = 'alert_header_reussi';
        contentalet = 'alert_content_reussi';
      } else {
        headeralert = '';
        contentalet = 'alert_insert_echec';
      }
      $translate(headeralert).then(function (header) {
        $translate(contentalet).then(function (content) {
          $ionicPopup.confirm({
            title: header,
            content: content,
            buttons: [
              {
                text: 'OK',
                type: 'button-energized',
                onTap: function (e) {
                  return true;
                }
              }]
          })
            .then(function (result) {
              if (!result) {

              } else {

              }
            });
        });

      });
    }
  })
  .factory('ChekConnect', function () {
    var connect;

    return {
      getConnectivite: function () {
        if (window.cordova) {
          if (window.Connection) {
            if (navigator.connection.type == Connection.NONE) {
              connect = false;
            }
            else {
              connect = true;
            }
          }
        } else {
          connect = true;
        }

        return connect;
      }
    }
  })
  .factory('urlPhp', function () {
    var connect;

    return {
      getUrl: function () {

        //return "http://mob-ismail.yosard.com/webservice";
        //return "http://54.39.119.103/webservice";
        return "http://3.134.243.233//webservice";
        // return "http://mob.yosard.com:89/webservice";
      }
    }
  })
  .factory('urlJava', function () {
    var connect;

    return {
      getUrl: function () {
        //return "http://ismail.yosard.com:8080/yup/rest";
        return "http://v-beta.yosard.com:8080/yup/rest";
        // return "http://www.yosard.com:8080/yup/rest";
      }
    }
  })
  .factory('ProfilUser', function () {
    var profil = 'limite';
    //$scope.data.profile = sessionStorage.getItem("")
    return {
      profilUser: function () {
        console.log("Le profils user")
        console.log(localStorage.getItem('loggedin_profil'))
        if (localStorage.getItem('loggedin_profil') == 'Codir YUP Mgt' || localStorage.getItem('loggedin_profil') == 'Direction Commerciale YUP Mgt'
          || localStorage.getItem('loggedin_profil') == 'Marketing YUP Mgt' || localStorage.getItem('loggedin_profil') == 'Call Center YUP Mgt'
          || localStorage.getItem('loggedin_profil') == 'Administrateur Maintenance') {
          // $scope.data.profile = 'super';
          profil = 'super';
        }
        return profil;
      }
    }
  })
  .factory('ListpaysByProfil', function ($http, urlPhp) {
    var connect;
    var listdespays;
    var payschoisit;
    var pays;
    return {
      getListpays: function (profil) {
        console.log(profil)
        if (profil == 'super') {
          var url = urlPhp.getUrl();
          $http.get(url + "/pays.php")
            .success(function (response) {
              // $ionicLoading.hide();
              pays = response;
              localStorage.setItem('paysOnline', angular.toJson(pays));
              listdespays = [];
              for (var i = 0; i < response.length; i++) {
                var pv = { name: response[i].pays, id: response[i].idpays }
                listdespays.push(pv);
              }
              console.log(listdespays)
            }).catch(function (error) {
              // $ionicLoading.hide();
              console.log(error)
            });
          //
        } else {
          //Recuperer la liste des pays
          var url = urlPhp.getUrl();
          $http.get(url + "/paysByUser.php?idutilisateurs=" + sessionStorage.getItem('loggedin_iduser'))
            .success(function (response) {
              //  $ionicLoading.hide();
              pays = response;
              localStorage.setItem('paysOnline', angular.toJson(pays));
              listdespays = [];
              for (var i = 0; i < response.length; i++) {
                var pv = { name: response[i].pays, id: response[i].idpays }
                listdespays.push(pv);
              }
              if (listdespays.length != 0) {
                payschoisit = listdespays[0];
              }
              listdespays = [];
              listdespays.push(payschoisit);
              console.log(listdespays)
              // $scope.listDesregionsByPaysID();
            }).catch(function (error) {
              // $ionicLoading.hide();
            });
          //Recuperer la liste des villes
        }

        return listdespays;
      }
    }
  })
  .factory('ApiModifPoint', function ($http, urlJava) {
    var result;
    return {
      updatePointvente: function (value) {

        var url = urlJava.getUrl();
        result = $http.post(url + "/yup/demande-point-vente/", value)
        return result;
      }
    }
  })
  .factory('ApiListPointventeParZone', function ($http, urlJava) {
    var result;
    return {
      pointventParZone: function (idpays, idzone) {
        var url = urlJava.getUrl();
        result = $http.get(url + "/yup/pays/" + idpays + "/zone/" + idzone + "/point-ventes")
        return result;
      }
    }
  })
  .factory('ApiListPays', function ($http, urlPhp) {
    var listdespays;
    return {
      getListpays: function (profil) {
        console.log(profil)
        if (profil == 'super') {
          var url = urlPhp.getUrl();
          listdespays = $http.get(url + "/pays.php")
        } else {

          var url = urlPhp.getUrl();
          listdespays = $http.get(url + "/paysByUser.php?idutilisateurs=" + sessionStorage.getItem('loggedin_iduser'))
        }

        return listdespays;
      }
    }
  })
  .factory('ApiListZonesSuper', function ($http, urlJava) {
    var listzones;
    return {
      getListZone: function (idpays) {

        var url = urlJava.getUrl();
        listzones = $http.get(url + "/yup/pays/" + idpays + "/zones")

        return listzones;
      }
    }
  })
  .factory('ApiListZonesLimite', function ($http, urlJava) {
    var listzones;
    return {
      getListZone: function (iduser) {

        var url = urlJava.getUrl();
        listzones = $http.get(url + "/yup/users/" + iduser + "/zones")

        return listzones;
      }
    }
  })
  .factory('ApiListRegion', function ($http, urlPhp) {
    var listRegions;
    return {
      getListRegionsByPays: function (idpays) {

        var url = urlPhp.getUrl();
        listRegions = $http.get(url + "/regionsByPays.php?idpays=" + idpays)
        return listRegions;
      }
    }
  })
  .factory('ApiListPointVente', function ($http, urlPhp) {
    var urlLocal = 'http://localhost/webservice';
    return {
      //$scope.pointventes = angular.fromJson(res.data).sort();

      getListPointVente: function (idUser, idRegions) {
        var url = urlPhp.getUrl();
        //   $ionicLoading.show({ content: 'Loading', animation: 'fade-in', showBackdrop: true, maxWidth: 200, showDelay: 0, duration: 10000 });
        return $http.get(url + '/pointventesutilisateurmap.php?idutilisateurs=' + idUser + "&idregions=" + idRegions);
      }
    }
  })
  .factory('ApiListPointVente', function ($http, urlPhp) {
    var urlLocal = 'http://localhost/webservice';
    return {
      //$scope.pointventes = angular.fromJson(res.data).sort();

      getListPointVente: function (idUser, idRegions) {
        var url = urlPhp.getUrl();
        //   $ionicLoading.show({ content: 'Loading', animation: 'fade-in', showBackdrop: true, maxWidth: 200, showDelay: 0, duration: 10000 });
        return $http.get(url + '/pointventesutilisateurmap.php?idutilisateurs=' + idUser + "&idregions=" + idRegions);
      }
    }
  })
  .factory('ApiListPointVenteJava', function ($http, urlJava) {
    return {
      //$scope.pointventes = angular.fromJson(res.data).sort();
      getListTypePointVente: function (idUser, idRegions) {
        var url = urlJava.getUrl();
        //   $ionicLoading.show({ content: 'Loading', animation: 'fade-in', showBackdrop: true, maxWidth: 200, showDelay: 0, duration: 10000 });
        return $http.get(url + '/yup/type-points-vente');
      }
    }
  })
  .factory('ApiListTypePointVente', function ($http, urlPhp) {
    return {
      //$scope.pointventes = angular.fromJson(res.data).sort();
      getListTypePointVente: function () {
        var urlLocal = 'http://localhost/webservice'
        var url = urlPhp.getUrl();
        //   $ionicLoading.show({ content: 'Loading', animation: 'fade-in', showBackdrop: true, maxWidth: 200, showDelay: 0, duration: 10000 });
        return $http.get(url + '/typepoint.php');
      }
    }
  })
  .factory('ModelTypePointVente', function () {
    var model = [];
    return {
      getModelTypePointVente: function (listtypepointvente) {
        console.log('brut')
        console.log(listtypepointvente)
        for (var i = 0; i < listtypepointvente.length; i++) {
          model.push(listtypepointvente[i]);
        }
        console.log('model')
        console.log(model)
        return model;
      }
    }
  })
  .factory('ModelPointVente', function () {
    var model = [];
    return {
      getModelPointVente: function (listpointvente) {
        console.log('brut')
        console.log(listpointvente)
        listpointvente.forEach(function (pv) {
          var m = { pointvente: pv.client, code: pv.codePointVente, telephone: pv.telephone, latitude: pv.latitude, longitude: pv.longitude, adresse: pv.adresse };
          model.push(m)

        });
        return model;
      }
    }
  })
  .factory('ModelRegions', function () {
    var model = [];
    return {
      getModelRegions: function (listRegions) {
        for (var i = 0; i < listRegions.length; i++) {
          var pv = { name: listRegions[i].region, id: listRegions[i].idregion }
          model.push(pv);
        }
        return model;
      }
    }
  })
  .factory('ModelPays', function () {
    var model;
    return {
      getModelPays: function (listPays) {
        model = [];
        for (var i = 0; i < listPays.length; i++) {
          var pv = { name: listPays[i].pays, id: listPays[i].idpays }
          model.push(pv);
        }
        return model;
      }
    }
  })
  .factory('Loading', function () {
    return {
      loadin: function () {
        // $ionicLoading.show({ content: 'Loading', animation: 'fade-in', showBackdrop: true, maxWidth: 200, showDelay: 0, duration: 10000 });
      }
    }
  })
